import sys

if len(sys.argv) > 1:
    if sys.argv[1] != "makemigrations" and sys.argv[1] != "migrate":
        default_app_config = 'mainsite.apps.XFMainAppConfig'