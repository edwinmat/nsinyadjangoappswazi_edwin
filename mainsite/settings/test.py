from .base import *

DEBUG = True

#ALLOWED_HOSTS = ['localhost','127.0.0.1','ptbi-dashboard.globalhealthapp.net']
ALLOWED_HOSTS = ['*']
#USE_X_FORWARDED_HOST = True

DATABASES = {
    'default': {

        'ENGINE': 'django.db.backends.postgresql',
        'OPTIONS': {
            'options': '-c search_path=nsinya_test'
        },
        'NAME': 'odk_nsinya_test',
        'USER': 'postgres',
        'PASSWORD': 'nsinya*.2017',
        'HOST': '192.168.168.79',
        'PORT': '5432',
    },
}

STATIC_ROOT = '/var/www/nsinya-django-app/static/'

STATIC_URL = '/static/'

