import os

from .base import *
from django.utils.translation import ugettext_lazy as _

DEBUG = True

DATABASES = {
    'default': {

        'ENGINE': 'django.db.backends.postgresql',
        'OPTIONS': {
            'options': '-c search_path=django'
        },
        'NAME': 'nsinya',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

STATIC_ROOT = '/var/www/nsinya-django-app/static/'

STATIC_URL = '/static/'

# LOGGING = {
#     'version': 1,
#     'loggers': {
#         'django.db.backends': {
#             'level': 'DEBUG',
#         },
#     },
# }
#
# INSTALLED_APPS += ('debug_toolbar',)
#
# MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

