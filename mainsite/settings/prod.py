from .base import *

DEBUG = False

#ALLOWED_HOSTS = ['localhost','127.0.0.1','ptbi-dashboard.globalhealthapp.net']
ALLOWED_HOSTS = ['*']
#USE_X_FORWARDED_HOST = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'OPTIONS': {
            'options': '-c search_path=django'
        },
        'NAME': 'nsinya',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5433',
    },
}

STATIC_ROOT = '/var/www/nsinya-prod.globalhealthapp.net/static/'

STATIC_URL = '/static/'


