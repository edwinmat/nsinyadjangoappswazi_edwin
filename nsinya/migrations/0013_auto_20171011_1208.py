# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 12:08
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('nsinya', '0012_auto_20171011_1205'),
    ]

    operations = [
        migrations.RenameField(
            model_name='worksheet',
            old_name='worksheet_samples',
            new_name='worksheet_sputum',
        ),
        migrations.AlterField(
            model_name='batch_transport',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='census',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genexpert',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genotype_cm',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genotype_mtbdr',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='hiv_screening',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='participant',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='participant_tracking',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='reception_in',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='reimbursment',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='risk_factor',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='smear',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='sputum_collection',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='symptom_screening',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='xray',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='ziehl',
            name='id',
            field=models.UUIDField(default=uuid.UUID('faf8c65f-7b23-4aa2-adc1-238b9453f46a'), editable=False, primary_key=True, serialize=False),
        ),
    ]
