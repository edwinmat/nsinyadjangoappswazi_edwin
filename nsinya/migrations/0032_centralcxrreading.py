# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-02 17:48
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nsinya', '0031_auto_20171102_1305'),
    ]

    operations = [
        migrations.CreateModel(
            name='CentralCXRReading',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(editable=False, max_length=40, null=True)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(editable=False, max_length=40, null=True)),
                ('cxr_reading_result', models.CharField(choices=[('0', 'Normal'), ('1', 'Anormal, Não sugestivo de TB pulmonar'), ('2', 'Anormal Sim, sugestivo de TB pulmonar '), ('7', 'Não interpretável')], max_length=1)),
                ('cavity', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('infiltrate', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('lung_nodules', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('pleural_effusion', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('mediastinal_lymphadenopathy', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('hilar_lymphadenopathy', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('discipleship', models.CharField(blank=True, choices=[(1, 'Esquerda'), (2, 'Direita')], max_length=1, null=True)),
                ('cxr_final_reading_result', models.CharField(blank=True, choices=[(1, 'Detectado Anormal - Nao significante'), (2, 'Detectado Anormal - significante - Sem doenca activa'), (3, 'Detectado anormal - significante - nao tuberculose'), (4, 'Detectado anormal - significante - tuberculose'), (5, 'Detectado anormal, significate - nao classificado')], max_length=1, null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_nsinya_centralcxrreading_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='modified_nsinya_centralcxrreading_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by')),
                ('participant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nsinya.Participant', verbose_name='Participant')),
            ],
            options={
                'db_table': 'nsinya_central_cxr_reading',
            },
        ),
    ]
