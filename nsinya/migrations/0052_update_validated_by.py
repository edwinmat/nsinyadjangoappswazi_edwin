# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-01-11 13:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('nsinya', '0051_add_validated_by'),
    ]

    operations = [
        migrations.RunSQL(
            "UPDATE nsinya_culture set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;"
            "UPDATE nsinya_genotype_cm set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;"
            "UPDATE nsinya_genotype_mtbdr set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;"
            "UPDATE nsinya_smear set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;"
            "UPDATE nsinya_tsa set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;"
            "UPDATE nsinya_ziehl set validated_by_id="
                "CASE "
                    "WHEN modified_by_id=4 THEN NULL "
                    "WHEN modified_by_id=2 THEN 25 "
                "ELSE modified_by_id END;")
    ]
