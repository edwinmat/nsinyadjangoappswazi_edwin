# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-02 13:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nsinya', '0029_auto_20171102_1302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='census',
            name='house_compartments',
            field=models.IntegerField(null=True),
        ),
    ]
