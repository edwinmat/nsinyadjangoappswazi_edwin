# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-02-05 09:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nsinya', '0054_add_tube'),
    ]

    operations = [
        migrations.AddField(
            model_name='centralcxrreading',
            name='creation_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='centralcxrreading',
            name='update_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='cluster',
            name='close_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='cluster',
            name='open_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='culture',
            name='update_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='other_household_member',
            name='creation_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='participant',
            name='creation_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='risk_factor',
            name='creation_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
