# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-05 20:47
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nsinya', '0035_auto_20171103_1806'),
    ]

    operations = [
        migrations.CreateModel(
            name='Collision_log',
            fields=[
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(editable=False, max_length=40, null=True)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(editable=False, max_length=40, null=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('sequence', models.IntegerField()),
                ('old_household', models.CharField(max_length=5)),
                ('new_household', models.CharField(max_length=5)),
                ('old_pin', models.CharField(max_length=7)),
                ('new_pin', models.CharField(max_length=7)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_nsinya_collision_log_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='modified_nsinya_collision_log_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by')),
            ],
            options={
                'get_latest_by': 'sequence',
            },
        ),
    ]
