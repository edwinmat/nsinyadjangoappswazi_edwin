# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-18 09:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nsinya', '0019_medical_review'),
    ]

    operations = [
        migrations.AlterField(
            model_name='culture',
            name='validated_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='culture',
            name='validation_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='genexpert',
            name='positivity',
            field=models.CharField(blank=True, choices=[('1', 'Alto'), ('2', 'Médio'), ('3', 'Baixo'), ('4', 'Muito baixo')], max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='genotype_mtbdr',
            name='validated_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='genotype_mtbdr',
            name='validation_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='smear',
            name='validated_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='smear',
            name='validation_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='tsa',
            name='validated_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='tsa',
            name='validation_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='ziehl',
            name='validated_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='ziehl',
            name='validation_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
