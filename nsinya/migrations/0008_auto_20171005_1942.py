# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-05 19:42
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nsinya', '0007_auto_20171003_1158'),
    ]

    operations = [
        migrations.CreateModel(
            name='Culture_reading',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(editable=False, max_length=40, null=True)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(editable=False, max_length=40, null=True)),
                ('sample_id', models.CharField(max_length=9)),
                ('positivity_week', models.CharField(choices=[('1', '24 Horas'), ('2', '48 Horas'), ('3', '1ª Semana'), ('4', '2ª Semana'), ('5', '3ª Semana'), ('6', '4ª Semana'), ('7', '5ª Semana'), ('8', '6ª Semana'), ('9', '7ª Semana'), ('10', '8ª Semana')], help_text='Tempo de positividade?', max_length=32)),
                ('culture_result', models.CharField(choices=[('1', 'Positivo'), ('2', 'Negativo'), ('3', 'Contaminada')], help_text='Resultados da Cultura?', max_length=32)),
                ('gradation', models.CharField(blank=True, choices=[('1', 'Contagem exata'), ('1', '+'), ('1', '++'), ('1', '+++')], help_text='Gradação?', max_length=32, null=True)),
                ('technician_name', models.CharField(max_length=100)),
                ('result_date', models.DateField()),
                ('validated_by', models.CharField(max_length=100, null=True)),
                ('validation_date', models.DateField(null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_nsinya_culture_reading_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Worksheet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(editable=False, max_length=40, null=True)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(editable=False, max_length=40, null=True)),
                ('worksheet_id', models.CharField(max_length=9)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_nsinya_worksheet_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='modified_nsinya_worksheet_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Worksheet_sample',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.RenameField(
            model_name='culture',
            old_name='result_date',
            new_name='inoculation_date',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='culture_result',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='gradation',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='positivity_week',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='sample_id',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='technician_name',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='validated_by',
        ),
        migrations.RemoveField(
            model_name='culture',
            name='validation_date',
        ),
        migrations.AlterField(
            model_name='batch_transport',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='census',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genexpert',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genotype_cm',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='genotype_mtbdr',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='hiv_screening',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='participant',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='participant_tracking',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='reception_in',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='reimbursment',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='risk_factor',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='smear',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='sputum_collection',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='symptom_screening',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='xray',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='ziehl',
            name='id',
            field=models.UUIDField(default=uuid.UUID('f92323fd-5e68-494d-9dc1-c102182d4a8e'), editable=False, primary_key=True, serialize=False),
        ),
        migrations.AddField(
            model_name='worksheet_sample',
            name='sample',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nsinya.Sputum_collection', verbose_name="Sample's ID"),
        ),
        migrations.AddField(
            model_name='worksheet_sample',
            name='worksheet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nsinya.Worksheet', verbose_name="Worksheet's ID"),
        ),
        migrations.AddField(
            model_name='culture_reading',
            name='culture',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nsinya.Culture', verbose_name="Culture's ID"),
        ),
        migrations.AddField(
            model_name='culture_reading',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='modified_nsinya_culture_reading_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by'),
        ),

    ]
