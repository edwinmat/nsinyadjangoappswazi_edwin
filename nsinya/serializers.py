from rest_framework import serializers
from .models import GeneXpert

class GeneXpertSerializer(serializers.ModelSerializer):

    class Meta:
        model = GeneXpert
        exclude = (
            'id',
            'created_by',
            'modified_by',
            'created_with_session_key',
            'modified_with_session_key',
            'mtb',
            'rif',
            'positivity',
            'gx_result'
        )