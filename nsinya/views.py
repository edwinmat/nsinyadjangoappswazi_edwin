# -*- coding: utf-8 -*-
import collections

import datetime

from .models import *
from django.core.urlresolvers import reverse
from .forms import BatchReceptionForm, SampleReceptionForm, CultureForm, TSAForm, MedicalReviewForm, \
    CentralCXRReadingForm
from django.contrib import messages
from django.shortcuts import redirect
from django.utils import timezone
from django.views.generic.base import TemplateView
from xf_system.views import XFNavigationViewMixin
from xf_crud.permission_mixin import XFPermissionMixin
from .security import *
from .api import cad4tb_utils

X_RAY = XRay

FIELD_PERSPECTIVE = "Field"

BATCH_LENGTH = 6

SAMPLE_LENGTH = 9

SELECT_RESULT = "Selecione pelo menos um resultado"


class ParticipantListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "census/participant_listing.html"

    # This is needed here because of the current design of xf_viz
    # def dispatch(self, request, *args, **kwargs):
    #     self.kwargs = {'slug':'participants'}
    #     return super(ParticipantListingView, self).dispatch(request)

    def get_context_data(self, **kwargs):
        self.context = context = super(ParticipantListingView, self).get_context_data(**kwargs)
        # self.ensure_group_or_403(CENSUS_TEAM)
        if is_field_perspective(self):
            try:
                cluster = Cluster.objects.get(status=OPEN)
                context['participants'] = Participant.objects.filter(pin__startswith=cluster.code)
            except Cluster.DoesNotExist:
                pass
        else:
            context['participants'] = Participant.objects.all()
        context['hide_name'] = hide_field(self=self, groups=['Radiology'])
        self.set_navigation_context()
        return context


class ParticipantDashboardView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def get_context_data(self, **kwargs):
        self.context = context = super(ParticipantDashboardView, self).get_context_data(**kwargs)

        na = 'N/A'

        participant = Participant.objects.get(pin=self.kwargs['pin'])
        context['participant'] = participant

        ri = Reception_in.objects.filter(participant=participant).first()
        context['reception_in'] = ri if ri is not None else na

        ss = Symptom_Screening.objects.filter(participant=participant).first()
        context['symptom_screening'] = ss if ss is not None else na

        rf = Risk_factor.objects.filter(participant=participant).first()
        context['risk_factors'] = rf if rf is not None else na

        xray = XRay.objects.filter(participant=participant).first()
        context['xray'] = xray if xray is not None else na
        context['cad4tb'] = xray.CAD4TB_score if xray is not None and xray.CAD4TB_score is not None else na

        hiv = HIV_Screening.objects.filter(participant=participant).first()
        context['hiv_screening'] = hiv if hiv is not None else na

        sc = Sputum_collection.objects.filter(participant=participant).first()
        context['sputum_collection'] = sc if sc is not None else na

        gx = GeneXpert.objects.filter(patientId=participant.pin).first()
        context['gene_xpert'] = gx if gx is not None else na

        reimbursement = Reimbursment.objects.filter(participant=participant).first()
        context['reimbursement'] = reimbursement if reimbursement is not None else na

        app = self.request.GET.get('app')
        if app is None:
            self.template_name = "census/participant_dashboard.html"
        else:
            self.template_name = "census/participant_dashboard_stripped.html"
        self.set_navigation_context()
        return context


class HouseholdListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "census/household_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(HouseholdListingView, self).get_context_data(**kwargs)
        if is_field_perspective(self):
            try:
                cluster = Cluster.objects.get(status=OPEN)
                context['households'] = Census.objects.filter(cluster_code=cluster.code)
            except Cluster.DoesNotExist:
                pass
        else:
            context['households'] = Census.objects.all()

        self.set_navigation_context()
        return context


class HouseholdDashboardView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "census/household_dashboard.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(HouseholdDashboardView, self).get_context_data(**kwargs)

        household = Census.objects.get(id=self.kwargs['household_id'])

        context['household'] = household
        context['members'] = Participant.objects.filter(census=household)

        self.set_navigation_context()
        return context


class SampleTrackingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "diagnosis/sample_tracking.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(SampleTrackingView, self).get_context_data(**kwargs)
        context['samples'] = Sputum_collection.objects.all()
        self.set_navigation_context()
        return context


class SampleDashboardView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "diagnosis/sample_tracking_dashboard.html"

    def get_context_data(self, **kwargs):
        na = 'N/A'
        self.context = context = super(SampleDashboardView, self).get_context_data(**kwargs)
        sample = Sputum_collection.objects.get(sample_id=self.kwargs['sample_id'])

        context['sample'] = sample
        context['batch'] = na

        bs = Batch_sample.objects.filter(sample_id=sample.sample_id).first()
        if bs is not None:
            batch = Batch_transport.objects.get(id=bs.batch_id)
            context['transport'] = batch if batch is not None else na

        reception = Reception_CRL.objects.filter(sample_id=sample.sample_id).first()
        context['reception'] = reception if reception is not None else na

        self.set_navigation_context()
        return context


class BatchReceptionView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/batch_reception.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(BatchReceptionView, self).get_context_data(**kwargs)
        # self.ensure_group_or_403("Field team lider")
        context['batches'] = Batch_transport.objects.all()
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        batch_id = self.request.POST.get('batchId').strip()

        if len(batch_id) != BATCH_LENGTH:
            messages.error(self.request, "Batch " + batch_id + " is not a valid code.")
            return redirect(reverse('nsinya:batch-reception'))

        try:
            batch = Batch_transport.objects.get(batch_id=batch_id)
            return redirect(reverse('nsinya:receive-batch', kwargs={'batch_id': batch.batch_id}))
        except Batch_transport.DoesNotExist:

            cluster_qs = Cluster.objects.filter(code=batch_id[0:2])

            if cluster_qs:
                return redirect(reverse('nsinya:receive-batch', kwargs={'batch_id': batch_id}))

            messages.error(self.request, "Batch " + batch_id + " does not exist.")
            self.set_navigation_context()
            return redirect(reverse('nsinya:batch-reception'))


class ReceiveBatchView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/receive_batch.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ReceiveBatchView, self).get_context_data(**kwargs)
        batch_qs = Batch_transport.objects.filter(batch_id=self.kwargs['batch_id'])
        form = BatchReceptionForm
        context['form'] = form
        context['batch'] = batch_qs.first() if batch_qs else Batch_transport(batch_id=self.kwargs['batch_id'])
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        form = BatchReceptionForm(request.POST)
        if form.is_valid():
            batch_qs = Batch_transport.objects.filter(batch_id=self.kwargs['batch_id'])
            self.object = form.save(commit=False)
            self.object.batch_id = batch_qs.first().batch_id if batch_qs else self.kwargs['batch_id']
            self.object.total_received_samples = 0
            self.object.received_by = request.user.username
            self.object.reception_date = timezone.now()
            self.object.save()
        else:
            messages.error(self.request, form.errors)
            self.set_navigation_context()
        return redirect(reverse('nsinya:receive-batch',
                                kwargs={
                                    'batch_id': batch_qs.first().batch_id if batch_qs else self.kwargs['batch_id']}))


class ReceiveSampleView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/receive_sample.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ReceiveSampleView, self).get_context_data(**kwargs)
        batch_qs = Batch_transport.objects.filter(batch_id=self.kwargs['batch_id'])
        form = SampleReceptionForm
        context['form'] = form
        context['batch'] = batch_qs.first() if batch_qs else Batch_transport(batch_id=self.kwargs['batch_id'])
        context['sample_id'] = self.kwargs.get('sample_id')
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        batch_qs = Batch_transport.objects.filter(batch_id=self.kwargs['batch_id'])
        sample_id = self.request.POST.get('sample_id')
        url = 'nsinya:receive-sample'
        kwargs = {'batch_id': batch_qs.first().batch_id if batch_qs else self.kwargs['batch_id']}

        if sample_id:
            sample_id = sample_id.strip()
            if len(sample_id) != SAMPLE_LENGTH:
                messages.error(self.request, "Sample " + sample_id + " is not a valid code.")
                return redirect(reverse('nsinya:receive-sample', kwargs={'batch_id': self.kwargs['batch_id']}))

            if sample_id[0:2] != self.kwargs['batch_id'][0:2]:
                messages.error(self.request,
                               "Sample " + sample_id + " doesnt belong to cluster " + self.kwargs['batch_id'][0:2])
                return redirect(reverse('nsinya:receive-sample', kwargs={'batch_id': self.kwargs['batch_id']}))

            if not is_sample_received(self=self, sample_id=sample_id):
                kwargs['sample_id'] = sample_id
        else:
            form = SampleReceptionForm(request.POST)
            if form.is_valid():
                self.object = form.save(commit=False)
                self.object.sample_id = self.kwargs.get('sample_id')
                self.object.batch_id = batch_qs.first().batch_id if batch_qs else self.kwargs['batch_id']
                self.object.received_by = request.user.username
                self.object.reception_date = timezone.now()
                self.object.save()
                reception = Batch_reception.objects.get(
                    batch_id=batch_qs.first().batch_id if batch_qs else self.kwargs['batch_id'])
                reception.total_received_samples = reception.total_received_samples + 1
                reception.save()
            else:
                messages.error(self.request, form.errors)

        return redirect(reverse(url, kwargs=kwargs))


class ReceiveOrphanSampleView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/receive_orphan_sample.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ReceiveOrphanSampleView, self).get_context_data(**kwargs)
        context['sample_id'] = self.kwargs.get('sample_id')
        form = SampleReceptionForm
        context['form'] = form
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id')
        url = 'nsinya:receive-orphan-sample'
        kwargs = {}

        if sample_id:
            sample_id = sample_id.strip()
            if not is_sample_received(self=self, sample_id=sample_id):
                kwargs['sample_id'] = sample_id
        else:
            form = SampleReceptionForm(request.POST)
            if form.is_valid():
                self.object = form.save(commit=False)
                self.object.sample_id = self.kwargs.get('sample_id')
                self.object.received_by = request.user.username
                self.object.reception_date = timezone.now()
                self.object.save()
            else:
                messages.error(self.request, form.errors)

        return redirect(reverse(url, kwargs=kwargs))


class CultureListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/culture_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CultureListingView, self).get_context_data(**kwargs)
        context['cultures'] = Culture.objects.all()
        form = CultureForm
        context['form'] = form
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        form = CultureForm(request.POST)
        if form.is_valid():
            self.object = form.save(commit=False)
            self.object.technician_name = request.user.username
            self.object.result_date = timezone.now()
            self.object.save()
            return redirect(reverse('nsinya:culture-listing'))

        else:
            messages.error(self.request, form.errors)
            return redirect(reverse('nsinya:culture-listing'))


class CultureReadingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/culture_reading.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CultureReadingView, self).get_context_data(**kwargs)
        sample_id = self.kwargs.get('sample_id')
        cultures = Culture.objects.filter(sample_id=sample_id).order_by('id')
        form = CultureForm(initial={'sample_id': sample_id})
        context['form'] = form
        context['cultures'] = cultures
        context['show_edit_button'] = is_in_groups(request=self.request, groups=[LAB_ADMINS])
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sampleid')
        url = 'nsinya:culture-reading'
        kwargs = {}

        if sample_id:
            sample_id = sample_id.strip()
            if is_sample(self=self, sample_id=sample_id):
                kwargs['sample_id'] = sample_id
        else:
            sample_id = self.kwargs.get('sample_id')
            kwargs['sample_id'] = sample_id
            form = CultureForm(request.POST)
            if form.is_valid():
                self.object = form.save(commit=False)
                try:
                    tube = self.object.tube
                    positivity = self.object.positivity_week
                    Culture.objects.get(sample_id=sample_id, tube=tube, positivity_week=positivity)
                    messages.error(self.request, "Leitura da positividade [%s] do tubo [%s] já foi feita" % (
                        dict(POSITIVITY_WEEK_CHOICES)[self.object.positivity_week], self.object.tube))
                except Culture.DoesNotExist:
                    self.object.technician_name = request.user.username
                    self.object.result_date = timezone.now()
                    self.object.save()
            else:
                messages.error(self.request, form.errors)
        return redirect(reverse(url, kwargs=kwargs))


class CultureEditView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/culture_edit.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CultureEditView, self).get_context_data(**kwargs)
        culture = Culture.objects.get(id=self.kwargs.get('culture_id'))
        if culture.validated_by is not None:
            messages.error(self.request, "Não é possível editar a leitura pois a mesma ja foi validada")
        context['form'] = CultureForm(instance=culture)
        context['culture'] = culture
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        id = self.kwargs['culture_id']
        culture = Culture.objects.get(id=id)
        url = 'nsinya:culture-edit'
        kwargs = {'culture_id': id}
        if culture.validated_by is None:
            form = CultureForm(request.POST, instance=culture)
            if form.is_valid():
                self.object = form.save(commit=False)
                tube = self.object.tube
                positivity = self.object.positivity_week
                existing = Culture.objects.filter(sample_id=culture.sample_id, tube=tube,
                                                  positivity_week=positivity).first()
                if existing and existing.id != culture.id:
                    messages.error(self.request, "Leitura da positividade [%s] do tubo [%s] já existe" % (
                        dict(POSITIVITY_WEEK_CHOICES)[positivity], tube))
                else:
                    self.object.update_date = timezone.now()
                    self.object.save()
                    url = 'nsinya:culture-reading'
                    kwargs = {'sample_id': culture.sample_id}
            else:
                messages.error(self.request, form.errors)
        return redirect(reverse(url, kwargs=kwargs))


class CultureValidationView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/culture_validation.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CultureValidationView, self).get_context_data(**kwargs)
        sample_id = self.kwargs.get('sample_id')

        if sample_id:
            cultures = Culture.objects.filter(sample_id=sample_id).order_by('positivity_week')
        else:
            cultures = Culture.objects.filter(validated_by=None).order_by('positivity_week')

        for culture in cultures:
            if culture.validated_by is None:
                context['show_validate_button'] = True
                break

        context['cultures'] = cultures
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sampleid').strip()
        url = 'nsinya:culture-validation'
        kwargs = {}

        if sample_id:
            if is_sample(self=self, sample_id=sample_id):
                kwargs['sample_id'] = sample_id

        return redirect(reverse(url, kwargs=kwargs))


class ValidateCultureView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/validate_culture.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ValidateCultureView, self).get_context_data(**kwargs)
        culture = Culture.objects.get(id=self.kwargs['culture_id'])
        context['culture'] = culture
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        self.context = context = super(ValidateCultureView, self).get_context_data(**kwargs)
        culture = Culture.objects.get(id=context['culture_id'])
        culture.validated_by = request.user
        culture.validation_date = timezone.now()
        culture.save()
        return redirect(reverse('nsinya:culture-validation', kwargs={'sample_id': culture.sample_id}))


class ValidateCulturesView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/validate_culture.html"

    # def get_context_data(self, **kwargs):
    #     self.context = context = super(ValidateCulturesView, self).get_context_data(**kwargs)
    #     self.set_navigation_context()
    #     return context

    def post(self, request, *args, **kwargs):
        kwargs = {}
        if 'sample_id' in self.kwargs:
            kwargs['sample_id'] = self.kwargs.get('sample_id')
        selected_id = request.POST.getlist('selected_culture_id')
        if not len(selected_id):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_id:
                culture = Culture.objects.get(id=id)
                culture.validated_by = request.user
                culture.validation_date = timezone.now()
                culture.save()

        return redirect(reverse('nsinya:culture-validation', kwargs=kwargs))


class TSAListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/tsa_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(TSAListingView, self).get_context_data(**kwargs)
        tsas = TSA.objects.all()
        context['tsas'] = tsas
        for tsa in tsas:
            if tsa.validated_by is None:
                context['show_validate_button'] = True
                break
        form = TSAForm
        context['form'] = form
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        form = TSAForm(request.POST)
        if form.is_valid():
            self.object = form.save(commit=False)
            self.object.technician_code = request.user.username
            self.object.result_date = timezone.now()
            self.object.save()
            return redirect(reverse('nsinya:tsa-listing'))

        else:
            messages.error(self.request, form.errors)
            return redirect(reverse('nsinya:tsa-listing'))


class TSACreateView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/tsa_create.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(TSACreateView, self).get_context_data(**kwargs)
        form = TSAForm
        context['form'] = form
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id')
        if sample_id:
            sample_id = sample_id.strip()
            try:
                TSA.objects.get(sample_id=sample_id)
                messages.error(self.request, "TSA for Sample " + sample_id + " already exists")
                return redirect(reverse('nsinya:create-tsa'))
            except TSA.DoesNotExist:
                try:
                    Sputum_collection.objects.get(sample_id=sample_id)
                except Sputum_collection.DoesNotExist:
                    messages.error(self.request, "Sample " + sample_id + " does not exist")
                    return redirect(reverse('nsinya:create-tsa'))
            return redirect(
                reverse('nsinya:create-tsa', kwargs={'sample_id': sample_id}))
        else:
            form = TSAForm(request.POST)
            if form.is_valid():
                self.object = form.save(commit=False)
                self.object.sample_id = self.kwargs.get('sample_id')
                self.object.technician_code = request.user.username
                self.object.result_date = timezone.now()
                self.object.save()
                return redirect(reverse('nsinya:tsa-listing'))
            else:
                messages.error(self.request, form.errors)
                return redirect(
                    reverse('nsinya:create-tsa', kwargs={'sample_id': sample_id}))
            return redirect(reverse('nsinya:tsa-listing'))


class TSAValidationView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def post(self, request, *args, **kwargs):
        selected_ids = request.POST.getlist('selected_ids')
        if not len(selected_ids):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_ids:
                tsa = TSA.objects.get(id=id)
                tsa.validated_by = request.user
                tsa.validation_date = timezone.now()
                tsa.save()
        return redirect(reverse('nsinya:tsa-listing'))


class ClusterActivationView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "config/cluster_activation.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ClusterActivationView, self).get_context_data(**kwargs)
        context['clusters'] = Cluster.objects.all()
        self.set_navigation_context()
        return context


class ClusterStatusUpdateView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "config/cluster_activation.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ClusterStatusUpdateView, self).get_context_data(**kwargs)
        context['clusters'] = Cluster.objects.all()
        code = self.kwargs['cluster_code']
        try:
            cluster = Cluster.objects.get(code=code)
            status = int(self.kwargs['status'])
            if status < 1 or status > 2:
                messages.error(self.request, "Invalid Status")
            else:
                if status == 1:
                    try:
                        Cluster.objects.get(status=OPEN)
                        messages.error(self.request, "Only one cluster can be OPEN at a time. Close the other first.")
                    except Cluster.DoesNotExist:
                        cluster.open()
                        messages.success(self.request, "Cluster opened successfully")
                else:
                    cluster.close()
                    messages.success(self.request, "Cluster closed successfully")
        except Cluster.DoesNotExist:
            messages.error(self.request, "Invalid Cluster")
        self.set_navigation_context()
        return context


class ClusterBatchesView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "config/cluster_batches.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ClusterBatchesView, self).get_context_data(**kwargs)
        cluster = Cluster.objects.get(id=self.kwargs['cluster_id'])
        context['cluster'] = cluster
        context['batches'] = Cluster_batch.objects.filter(cluster=cluster)
        self.set_navigation_context()
        return context


class GeneXpertListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "diagnosis/gx_result_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(GeneXpertListingView, self).get_context_data(**kwargs)
        context['gx_results'] = GeneXpert.objects.all()
        self.set_navigation_context()
        return context


class SmearListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/smear_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(SmearListingView, self).get_context_data(**kwargs)

        if 'sample_id' in self.kwargs:
            smears = Smear.objects.filter(sample_id=self.kwargs['sample_id'])
        else:
            smears = Smear.objects.filter(validated_by=None)
        context['smears'] = smears
        for smear in smears:
            if smear.validated_by is None:
                context['show_validate_button'] = True
                break
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id').strip()

        if sample_id == '':
            return redirect(reverse('nsinya:smear-listing'))

        return redirect(reverse('nsinya:smear-listing', kwargs={'sample_id': sample_id}))


class ValidateSmearView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def post(self, request, *args, **kwargs):
        kwargs = {}
        if 'sample_id' in self.kwargs:
            kwargs['sample_id'] = self.kwargs.get('sample_id')
        selected_ids = request.POST.getlist('selected_ids')
        if not len(selected_ids):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_ids:
                smear = Smear.objects.get(id=id)
                smear.validated_by = request.user
                smear.validation_date = timezone.now()
                smear.save()
        return redirect(reverse('nsinya:smear-listing', kwargs=kwargs))


class WorksheetView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/worksheet_creation.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(WorksheetView, self).get_context_data(**kwargs)

        context['worksheet_types'] = Worksheet._meta.get_field('type').choices

        if 'type' in self.kwargs:
            context['ws_selected'] = self.kwargs['type']
            context['find_sample_visible'] = 'true' if self.kwargs['type'] == '6' else 'none'
            samples = self.generate_automatic_worksheet(context)
            context['samples'] = samples

            for sample in samples:
                self.request.session['sputum_list'] = self.request.session['sputum_list'] + "\n" + sample.sample_id

        if 'remove' not in self.kwargs and 'sample_id' not in self.kwargs and 'type' not in self.kwargs:
            self.request.session['sputum_list'] = ''

        if 'remove' in self.kwargs:
            samples = removeSputumFromSession(self, self.kwargs['sample_id'])
            context['samples'] = samples
            return context

        if 'sample_id' in self.kwargs:
            samples = getSputumInSession(self)
            context['samples'] = samples
            reception_crl = Reception_CRL.objects.filter(sample_id=self.kwargs['sample_id'])

            if reception_crl:

                if Worksheet_Sample.objects.filter(sample_id=reception_crl.first().sample_id).exists():
                    worksheet_sample = Worksheet_Sample.objects.distinct().filter(
                        sample_id=reception_crl.first().sample_id)
                    messages.error(self.request,
                                   "A sample " + self.kwargs[
                                       'sample_id'] + " ja existe na worksheet " + worksheet_sample.first().worksheet_code)
                    return context

            samples = samples | reception_crl
            context['samples'] = samples
            self.request.session['sputum_list'] = self.request.session['sputum_list'] + "\n" + self.kwargs['sample_id']

        self.set_navigation_context()
        return context

    def generate_automatic_worksheet(self, context):

        samples = Reception_CRL.objects.none()

        if self.kwargs['type'] == WORKSHEET_TYPE_1:
            culture_qs = Culture.objects.filter(culture_result=CULTURE_RESULT_1)
            for culture in culture_qs:
                if not Ziehl.objects.filter(sample_id=culture.sample_id) \
                        and self.can_proceed(culture.sample_id, self.kwargs['type']):
                    samples = samples | Reception_CRL.objects.filter(sample_id=culture.sample_id)

        if self.kwargs['type'] == WORKSHEET_TYPE_2:
            ziehl_qs = Ziehl.objects.filter(result=ZIEHL_1)
            for ziehl in ziehl_qs:
                if not Genotype_CM.objects.filter(sample_id=ziehl.sample_id) \
                        and self.can_proceed(ziehl.sample_id, self.kwargs['type']):
                    samples = samples | Reception_CRL.objects.filter(sample_id=ziehl.sample_id)

        if self.kwargs['type'] == WORKSHEET_TYPE_3:
            genotype_qs = Genotype_CM.objects.filter(result=GENOTYPE_CM_13)
            for genotype in genotype_qs:
                if not Genotype_MTBDR.objects.filter(sample_id=genotype.sample_id) \
                        and self.can_proceed(genotype.sample_id, self.kwargs['type']):
                    samples = samples | Reception_CRL.objects.filter(sample_id=genotype.sample_id)

        if self.kwargs['type'] == WORKSHEET_TYPE_4:
            culture_qs = Culture.objects.filter(culture_result=(CULTURE_RESULT_1, CULTURE_RESULT_2))
            for culture in culture_qs:
                if (Ziehl.objects.filter(sample_id=culture.sample_id, result=ZIEHL_2) or
                        Genotype_CM.objects.filter(sample_id=culture.sample_id, result=GENOTYPE_CM_16)
                        and (not GeneXpert.objects.filter(sample_id=culture.sample_id))):
                    if self.can_proceed(culture.sample_id, self.kwargs['type']):
                        samples = samples | Reception_CRL.objects.filter(sample_id=genotype.sample_id)

        if self.kwargs['type'] == WORKSHEET_TYPE_5:
            genotype_qs = Genotype_MTBDR.objects.filter(result=GENOTYPE_MT_2)
            for genotype in genotype_qs:
                if not TSA.objects.filter(sample_id=genotype.sample_id) \
                        and self.can_proceed(genotype.sample_id, self.kwargs['type']):
                    samples = samples | Reception_CRL.objects.filter(sample_id=genotype.sample_id)

        return samples

    def can_proceed(self, sample_id, type):

        worksheet_sample = Worksheet_Sample.objects.filter(sample_id=sample_id)

        for ws in worksheet_sample:
            if Worksheet.objects.filter(code=ws.worksheet_code, type=type):
                return False

        return True

    def post(self, request, *args, **kwargs):

        context = super(WorksheetView, self).get_context_data(**kwargs)
        samples = getSputumInSession(self)

        if 'save' in request.POST:

            code = "WS" + str(datetime.date.today().year) + str(datetime.date.today().month).zfill(2) + str(
                datetime.date.today().day).zfill(2) + "-"
            worksheet_qs = Worksheet.objects.filter(code__contains=code)
            next_sequence = 0

            if worksheet_qs:
                next_sequence = worksheet_qs.latest('sequence').sequence + 1
                code = code + str(next_sequence)
            else:
                next_sequence = next_sequence + 1
                code = code + str(next_sequence)

            Worksheet.objects.create(code=code, sequence=next_sequence, inoculation_date=datetime.date.today(),
                                     type=self.request.POST.get('worksheet_type'))
            Worksheet_Sample.objects.create(worksheet_code=code, sample_id="CTRL+")
            Worksheet_Sample.objects.create(worksheet_code=code, sample_id="CTRL-")

            for sputum in samples:
                Worksheet_Sample.objects.create(sample_id=sputum.sample_id, worksheet_code=code)

            messages.info(self.request, "Worksheet with code " + code + " created sucessfully")

            return redirect(reverse('nsinya:worksheet-details', kwargs={'worksheet_code': code}))

        sample_id = self.request.POST.get('sample_id').strip()
        type = self.request.POST.get('worksheet_type')

        if sample_id == '':
            return redirect(reverse('nsinya:worksheet-creation'))

        if type == None:
            return redirect(reverse('nsinya:worksheet-creation', kwargs={'sample_id': sample_id}))

        return redirect(reverse('nsinya:worksheet-creation', kwargs={'sample_id': sample_id, 'type': type}))


class WorksheetDetailsView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/worksheet_creation_details.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(WorksheetDetailsView, self).get_context_data(**kwargs)

        context['worksheet_samples'] = Worksheet_Sample.objects.filter(worksheet_code=self.kwargs['worksheet_code'])
        context['samples'] = ''
        self.set_navigation_context()
        return context


def removeSputumFromSession(self, sample_id):
    samples = Reception_CRL.objects.none()

    sputum_list = self.request.session['sputum_list']
    array = sputum_list.split("\n")
    self.request.session['sputum_list'] = ''

    for item in array:
        if item != sample_id and item != '':
            sputum = Reception_CRL.objects.filter(sample_id=item)
            samples = samples | sputum
            self.request.session['sputum_list'] = self.request.session['sputum_list'] + "\n" + item

    return samples


def getSputumInSession(self):
    samples = Reception_CRL.objects.none()

    sputum_list = self.request.session['sputum_list']
    array = sputum_list.split("\n")

    for item in array:
        if item != '':
            sputum = Reception_CRL.objects.filter(sample_id=item)
            samples = samples | sputum

    return samples


class WorksheetListView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/worksheet_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(WorksheetListView, self).get_context_data(**kwargs)

        context["worksheet_list"] = Worksheet.objects.all()

        if 'worksheet_id' in self.kwargs:
            worksheet = Worksheet.objects.filter(id=self.kwargs['worksheet_id'])
            ws_selected = self.kwargs['worksheet_id']

            context["ws_selected"] = ws_selected

            if worksheet:
                context["worksheet_samples"] = Worksheet_Sample.objects.filter(worksheet_code=worksheet.first().code)
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        worksheet_id = self.request.POST.get('worksheet_id')

        return redirect(reverse('nsinya:worksheet-list', kwargs={'worksheet_id': worksheet_id}))


class WorksheetEditView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/worksheet_edit.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(WorksheetEditView, self).get_context_data(**kwargs)
        self.set_navigation_context()

        if 'remove' in self.kwargs:
            Worksheet_Sample.objects.get(sample_id=self.kwargs['sample_id']).delete()

        worksheet = Worksheet.objects.get(id=self.kwargs['worksheet_id'])
        context['worksheet'] = worksheet
        context["worksheet_samples"] = Worksheet_Sample.objects.filter(worksheet_code=worksheet.code).exclude(
            sample_id='CTRL-').exclude(sample_id='CTRL+')

        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id')
        worksheet_id = self.kwargs['worksheet_id']
        worksheet = Worksheet.objects.get(id=worksheet_id)

        qs_ws = Worksheet_Sample.objects.filter(sample_id=sample_id)

        if qs_ws:
            messages.error(self.request,
                           'O sample com codigo ' + sample_id + ' já existe na worksheet ' + qs_ws.first().worksheet_code)
            return redirect(reverse('nsinya:worksheet-edit', kwargs={'worksheet_id': worksheet_id}))

        cluster_code = sample_id[0:2]
        qs_cluster = Cluster.objects.filter(code=cluster_code)

        if not qs_cluster:
            messages.error(self.request, 'O sample ' + sample_id + ' tem codigo de cluster invalido: ' + cluster_code)
            return redirect(reverse('nsinya:worksheet-edit', kwargs={'worksheet_id': worksheet_id}))

        if len(sample_id) != 9:
            messages.error(self.request, 'Codigo do sample invalido: tamanho diferente de nove(9)')
            return redirect(reverse('nsinya:worksheet-edit', kwargs={'worksheet_id': worksheet_id}))

        Worksheet_Sample.objects.create(sample_id=sample_id, worksheet_code=worksheet.code)
        messages.success(self.request, 'O sample ' + sample_id + ' foi adicionado com sucesso')

        return redirect(reverse('nsinya:worksheet-edit', kwargs={'worksheet_id': worksheet_id}))


class WorksheetInoculationView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/worksheet_inoculation.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(WorksheetInoculationView, self).get_context_data(**kwargs)

        context["worksheet_list"] = Worksheet.objects.all()

        if 'worksheet_id' in self.kwargs:
            worksheet = Worksheet.objects.filter(id=self.kwargs['worksheet_id'])
            ws_selected = self.kwargs['worksheet_id']

            context["ws_selected"] = ws_selected

            if worksheet.count() > 0:
                context["worksheet_samples"] = Worksheet_Sample.objects.filter(worksheet_code=worksheet.first().code)
        self.set_navigation_context()
        return context


class Genotype_CMListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/genotype_cm_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(Genotype_CMListingView, self).get_context_data(**kwargs)

        if 'sample_id' in self.kwargs:
            genotypes = Genotype_CM.objects.filter(sample_id=self.kwargs['sample_id'])
        else:
            genotypes = Genotype_CM.objects.filter(validated_by=None)

        context['genotype_cms'] = genotypes
        for genotype in genotypes:
            if genotype.validated_by is None:
                context['show_validate_button'] = True
                break

        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id').strip()

        if sample_id == '':
            return redirect(reverse('nsinya:genotytpe_cm-listing'))

        return redirect(reverse('nsinya:genotype_cm-listing', kwargs={'sample_id': sample_id}))


class ValidateGenotypeCmView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def post(self, request, *args, **kwargs):
        kwargs = {}
        if 'sample_id' in self.kwargs:
            kwargs['sample_id'] = self.kwargs.get('sample_id')
        selected_ids = request.POST.getlist('selected_ids')
        if not len(selected_ids):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_ids:
                genotype_cm = Genotype_CM.objects.get(id=id)
                genotype_cm.validated_by = request.user
                genotype_cm.validation_date = timezone.now()
                genotype_cm.save()
        return redirect(reverse('nsinya:genotype_cm-listing', kwargs=kwargs))


class Genotype_MTListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/genotype_mt_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(Genotype_MTListingView, self).get_context_data(**kwargs)

        if 'sample_id' in self.kwargs:
            genotypes = Genotype_MTBDR.objects.filter(sample_id=self.kwargs['sample_id'])
        else:
            genotypes = Genotype_MTBDR.objects.filter(validated_by=None)

        context['genotype_mtdbs'] = genotypes
        for genotype in genotypes:
            if genotype.validated_by is None:
                context['show_validate_button'] = True
                break
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id').strip()

        if sample_id == '':
            return redirect(reverse('nsinya:genotytpe_mt-listing'))

        return redirect(reverse('nsinya:genotype_mt-listing', kwargs={'sample_id': sample_id}))


class ValidateGenotypeMtView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def post(self, request, *args, **kwargs):
        kwargs = {}
        if 'sample_id' in self.kwargs:
            kwargs['sample_id'] = self.kwargs.get('sample_id')
        selected_ids = request.POST.getlist('selected_ids')
        if not len(selected_ids):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_ids:
                genotype_mt = Genotype_MTBDR.objects.get(id=id)
                genotype_mt.validated_by = request.user
                genotype_mt.validation_date = timezone.now()
                genotype_mt.save()
        return redirect(reverse('nsinya:genotype_mt-listing', kwargs=kwargs))


class ZiehlListingView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "crl/ziehl_listing.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(ZiehlListingView, self).get_context_data(**kwargs)

        if 'sample_id' in self.kwargs:
            ziehls = Ziehl.objects.filter(sample_id=self.kwargs['sample_id'])
        else:
            ziehls = Ziehl.objects.filter(validated_by=None)

        context['ziehls'] = ziehls
        for ziehl in ziehls:
            if ziehl.validated_by is None:
                context['show_validate_button'] = True
                break
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        sample_id = self.request.POST.get('sample_id').strip()
        if sample_id == '':
            return redirect(reverse('nsinya:ziehl-listing'))

        return redirect(reverse('nsinya:ziehl-listing', kwargs={'sample_id': sample_id}))


class ValidateZiehlView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):

    def post(self, request, *args, **kwargs):
        kwargs = {}
        if 'sample_id' in self.kwargs:
            kwargs['sample_id'] = self.kwargs.get('sample_id')
        selected_ids = request.POST.getlist('selected_ids')
        if not len(selected_ids):
            messages.error(self.request, SELECT_RESULT)
        else:
            for id in selected_ids:
                ziehl = Ziehl.objects.get(id=id)
                ziehl.validated_by = request.user
                ziehl.validation_date = timezone.now()
                ziehl.save()

        return redirect(reverse('nsinya:ziehl-listing', kwargs=kwargs))


class MedicalReviewPanelView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "diagnosis/medical_review_panel.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(MedicalReviewPanelView, self).get_context_data(**kwargs)
        context['participants'] = Participant.objects.all()
        self.set_navigation_context()
        return context


class CentralCXRReadingList(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "radiology/central_cxr_reading_list.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CentralCXRReadingList, self).get_context_data(**kwargs)
        context['participants'] = Participant.objects.filter(reception_in__participant__isnull=False,
                                                             xray__xray_status__exact='1',
                                                             reimbursment__participant__isnull=False)
        self.set_navigation_context()
        return context


class CentralCXRReading(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "radiology/central_cxr_reading.html"

    def get_context_data(self, **kwargs):

        self.context = context = super(CentralCXRReading, self).get_context_data(**kwargs)

        na = 'N/A'

        participant = Participant.objects.get(pin=self.kwargs['pin'])
        context['participant'] = participant

        ss = Symptom_Screening.objects.filter(participant=participant).first()
        context['symptom_screening'] = ss if ss is not None else na

        rf = Risk_factor.objects.filter(participant=participant).first()
        context['risk_factors'] = rf if rf is not None else na

        xray = XRay.objects.filter(participant=participant).first()
        context['xray'] = xray if xray is not None else na

        hiv = HIV_Screening.objects.filter(participant=participant).first()
        context['hiv_screening'] = hiv if hiv is not None else na

        central_cxr_reading_instance = participant.central_cxr_reading

        if central_cxr_reading_instance:
            form = CentralCXRReadingForm(instance=central_cxr_reading_instance)
        else:
            form = CentralCXRReadingForm()

        self.set_navigation_context()
        context['form'] = form

        return context

    def post(self, request, *args, **kwargs):
        pin = self.kwargs['pin']
        participant = Participant.objects.get(pin=pin)
        central_cxr_reading_instance = participant.central_cxr_reading
        if central_cxr_reading_instance:
            form = CentralCXRReadingForm(request.POST, instance=central_cxr_reading_instance)
        else:
            form = CentralCXRReadingForm(request.POST)
        if form.is_valid():
            self.object = form.save(commit=False)
            if not central_cxr_reading_instance:
                self.object.participant_id = participant.id
                self.object.creation_date = timezone.now()
            else:
                self.object.update_date = timezone.now()
            self.object.save()
        else:
            messages.error(self.request, form.errors)
            return redirect(reverse('nsinya:central_cxr_reading', kwargs={'pin': pin}))
        return redirect(reverse('nsinya:central-cxr-reading-list'))


class ReviewParticipantView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "diagnosis/review_participant.html"

    def get_context_data(self, **kwargs):

        self.context = context = super(ReviewParticipantView, self).get_context_data(**kwargs)

        na = 'N/A'

        participant = Participant.objects.get(pin=self.kwargs['pin'])
        context['participant'] = participant

        ss = Symptom_Screening.objects.filter(participant=participant).first()
        context['symptom_screening'] = ss if ss is not None else na

        rf = Risk_factor.objects.filter(participant=participant).first()
        context['risk_factors'] = rf if rf is not None else na

        xray = XRay.objects.filter(participant=participant).first()
        context['xray'] = xray if xray is not None else na

        hiv = HIV_Screening.objects.filter(participant=participant).first()
        context['hiv_screening'] = hiv if hiv is not None else na

        form = MedicalReviewForm()
        context['form'] = form

        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        pin = self.kwargs['pin']

        form = MedicalReviewForm(request.POST)
        if form.is_valid():
            self.object = form.save(commit=False)
            self.object.participant_id = pin
            self.object.panel_representative = request.user.username
            self.object.review_date = timezone.now()
            self.object.save()
        else:
            messages.error(self.request, form.errors)
            return redirect(reverse('nsinya:review-participant', kwargs={'pin': pin}))
        return redirect(reverse('nsinya:medical-review_panel'))


class CollisionsDashboardView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "census/collisions_dashboard.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CollisionsDashboardView, self).get_context_data(**kwargs)
        context['collisions'] = Collision_log.objects.all()
        self.set_navigation_context()
        return context


class CheckEligibilityView(TemplateView, XFPermissionMixin, XFNavigationViewMixin):
    template_name = "census/check_eligibility.html"

    def get_context_data(self, **kwargs):
        self.context = context = super(CheckEligibilityView, self).get_context_data(**kwargs)
        if 'pin' in self.kwargs:
            pin = self.kwargs.get('pin')
            try:
                participant = Participant.objects.get(pin=pin)
                xray = XRay.objects.filter(participant=participant).first()
                if xray:
                    cad4tb_utils.update_xray_by_cxr_reading(xray=xray, pin=pin)
                    run_decision_algorithm(self=self, participant=participant, xray=xray)

                    if participant.eligible_for_sputum is None:
                        if not xray.xray_results:
                            messages.error(self.request, "Participante ainda não tem leitura de Raio X submetida")
                        elif xray.xray_results == XRAY_RESULTS_4:
                            messages.error(self.request, "Não interpretável (Repetir Rx)")
                        elif not xray.CAD4TB_score:
                            messages.error(self.request, "Participante ainda não tem CAD4TB")
                    else:
                        context['xray'] = xray
                        if participant.eligible_for_sputum == YES:
                            messages.success(self.request, "Participante elegivel para colheita de amostra")
                        else:
                            messages.error(self.request, "Participante não elegivel para colheita de amostra")
                else:
                    messages.error(self.request, "Participante com PIN %s não passou pela estação de Raio X" % pin)

            except Participant.DoesNotExist:
                messages.error(self.request, "Participante com PIN %s nao existe no sistema" % pin)
        self.set_navigation_context()
        return context

    def post(self, request, *args, **kwargs):
        return redirect(reverse('nsinya:check-eligibility', kwargs={'pin': self.request.POST.get('pin').strip()}))


def is_sample(self, sample_id):
    try:
        reception = Reception_CRL.objects.get(sample_id=sample_id)
    except Reception_CRL.DoesNotExist:
        messages.error(self.request, "Não foi encontrada nenhuma amostra com código %s" % sample_id)
        return None
    return reception


def is_sample_received(self, sample_id):
    try:
        Reception_CRL.objects.get(sample_id=sample_id)
        messages.error(self.request, "Amostra %s ja foi recebida" % sample_id)
    except Reception_CRL.DoesNotExist:
        return False
    return True


def is_sample_in_batch(self, sample_id, batch_id):
    try:
        Batch_sample.objects.get(batch_id=batch_id, sample_id=sample_id)
    except Batch_sample.DoesNotExist:
        messages.error(self.request, "A amostra %s não pertence ao lote %s" % (sample_id, batch_id))
        return False
    return True


def is_field_perspective(self):
    user_perspective = self.request.user.profile.default_perspective
    if user_perspective and user_perspective.name == FIELD_PERSPECTIVE:
        return True
    return False


def hide_field(self, groups):
    if not len(groups):
        return False
    for group in groups:
        print(group)
        if self.request.user.groups.filter(name=group).exists():
            return True
    return False


def get_CXR(xray: XRay):
    if xray.CAD4TB_score is None:
        return None
    if xray.CAD4TB_score >= 40:
        return 1
    else:
        if xray.xray_results == XRAY_RESULTS_2:
            return 1
        else:
            return 0


def run_decision_algorithm(self, participant: Participant, xray: XRay):
    if participant.eligible_for_sputum and participant.eligible_for_sputum != '':
        pass
    else:
        eligibility = None
        if xray.xray_status != YES or xray.xray_results == XRAY_RESULTS_4:
            eligibility = YES
        else:
            cxr = get_CXR(xray=xray)
            if cxr is not None:
                eligibility = YES
                try:
                    ss = Symptom_Screening.objects.get(participant=participant)
                    if cxr == 0 and ss.symptom_eligible == NO:
                        eligibility = NO
                except Symptom_Screening.DoesNotExist:
                    pass

        participant.eligible_for_sputum = eligibility
        participant.save()
