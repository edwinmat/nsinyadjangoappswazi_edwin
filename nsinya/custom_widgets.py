from django import forms


class HorizontalRadioSelect(forms.RadioSelect):
    template_name = 'widgets/xf_horizontal_radio_select.html'
