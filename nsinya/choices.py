# -*- coding: utf-8 -*-

BLANK = '99'
DONTKNOW = '9'
XRAYDENIED = '3'

# Gender Choices
MALE = '1'
FEMALE = '2'

GENDER_CHOICES = (
    (MALE, 'Masculino'),
    (FEMALE, 'Feminino'),
    (BLANK, 'Não especificado'),
)

# Residence area choices
RURAL = '1'
URBAN = '2'

RESIDENCE_AREA_CHOICES = (
    (RURAL, 'Rural'),
    (URBAN, 'Urbana'),
    (BLANK, 'Não especificado'),
)

# Academic level
NONE_ACADEMIC = '1'
PRIMARY_LEVEL = '2'
SECONDARY_LEVEL = '3'
HIGHER_LEVEL = '4'

ACADEMIC_LEVEL_CHOICES = (
    (NONE_ACADEMIC, 'Nenhum'),
    (PRIMARY_LEVEL, 'Primário'),
    (SECONDARY_LEVEL, 'Secundário'),
    (HIGHER_LEVEL, 'Superior'),
)

# Marital status
MARRIED = '1'
DIVORCED = '2'
WIDOWED = '3'
SINGLE = '4'
DKNOW_MARITAL = '8'

MARITAL_STATUS_CHOICES = (
    (MARRIED, 'Casado(a)	ou vive em união'),
    (DIVORCED, 'Divorciado ou Separado'),
    (WIDOWED, 'Viúvo(a)'),
    (SINGLE, 'Nunca esteve casado(a) e nunca viveu em união'),
    (DKNOW_MARITAL, 'Não sabe')
)

# House Hold Types
HOUSE = '1'
SCHOOL = '2'
HOSPITAL = '3'
PLANTATION = '4'
OTHER = '5'

HOUSEHOLD_TYPE_CHOICES = (
    (HOUSE, 'Casa'),
    (SCHOOL, 'Escola'),
    (HOSPITAL, 'Hospital'),
    (OTHER, 'Outro'),
)

# Yes or No Choices
YES = '1'
NO = '2'

YESNO_CHOICES = (
    (YES, 'Sim'),
    (NO, 'Não'),
)

YESNODONTKNOW_CHOICES = (
    (YES, 'Sim'),
    (NO, 'Não'),
    (DONTKNOW, 'Não Sabe//Não respondeu'),
)

# Household residence permanence type choices
PERMANENT = 'P'
TEMPORARY = 'T'

RESIDENCE_PERMANENCE_CHOICES = (
    (PERMANENT, 'Permanente'),
    (TEMPORARY, 'Temporario'),
)

# Household relationships choices
HEAD_OF_HOUSEHOLD = '1'
SPOUSE = '2'
CHILD = '3'
PARENT = '4'
BROTHER_SISTER = '5'
GRAND_CHILD = '6'
FATHER_MOTHER_IN_LAW = '7'
SON_DAUGHTER_IN_LAW = '8'
ADOPTIVE_CHILD_STEPCHILD = '9'
ANOTHER_RELATIVE = '10'
NO_RELATION = '11'

HOUSEHOLD_RELATIONSHIPS_CHOICES = (
    (HEAD_OF_HOUSEHOLD, 'Chefe do Agregado'),
    (SPOUSE, 'Conjuge'),
    (CHILD, 'Filho/Filha'),
    (BROTHER_SISTER, 'Irmão/Irmã'),
    (GRAND_CHILD, 'Neto/Neta'),
    (FATHER_MOTHER_IN_LAW, 'Sogro/Sogra'),
    (SON_DAUGHTER_IN_LAW, 'Genro/Nora'),
    (ADOPTIVE_CHILD_STEPCHILD, 'Filho adoptivo/enteado'),
    (ANOTHER_RELATIVE, 'Outro Parente'),
    (NO_RELATION, 'Sem parentesco'),
    (BLANK, 'Não especificado'),
)

# House ownership type
HOUSE_OWN = '1'
HOUSE_RENT = '2'
HOUSE_LEND = '3'
HOUSE_OTHER = '88'

HOUSE_OWNERSHIP_TYPE = (
    (HOUSE_OWN, 'Propria'),
    (HOUSE_RENT, 'Alugada'),
    (HOUSE_LEND, 'Cedida/emprestada temporariamente'),
    (HOUSE_OTHER, 'Outra'),
)

# Building Material
BUILDING_MATERIAL_1 = '1'
BUILDING_MATERIAL_2 = '2'
BUILDING_MATERIAL_3 = '3'
BUILDING_MATERIAL_4 = '4'
BUILDING_MATERIAL_5 = '5'
BUILDING_MATERIAL_6 = '6'
BUILDING_MATERIAL_7 = '7'
BUILDING_MATERIAL_8 = '88'

BUILDING_MATERIAL_CHOICES = (
    (BUILDING_MATERIAL_1, 'Bloco de cimento'),
    (BUILDING_MATERIAL_2, 'Bloco de tijolo'),
    (BUILDING_MATERIAL_3, 'Madeira e zinco'),
    (BUILDING_MATERIAL_4, 'Adobe/Bloco de adobe'),
    (BUILDING_MATERIAL_5, 'Canico, Paus/Bambus/Palmeiras'),
    (BUILDING_MATERIAL_6, 'Paus maticados'),
    (BUILDING_MATERIAL_7, 'Lata/Cartao/Papel/Saco/Casca'),
    (BUILDING_MATERIAL_8, 'Outro'),
)

# Covering material
COVERING_MATERIAL_1 = '1'
COVERING_MATERIAL_2 = '2'
COVERING_MATERIAL_3 = '3'
COVERING_MATERIAL_4 = '4'
COVERING_MATERIAL_5 = '88'

COVERING_MATERIAL_CHOICES = (
    (COVERING_MATERIAL_1, 'Laje de betao'),
    (COVERING_MATERIAL_2, 'Telha'),
    (COVERING_MATERIAL_3, 'Chapa de Zinco'),
    (COVERING_MATERIAL_4, 'Capim/colmo/palmeira'),
    (COVERING_MATERIAL_5, 'Outro'),
)

# House owning method
HOUSE_OWNING_METHOD_1 = '1'
HOUSE_OWNING_METHOD_2 = '2'
HOUSE_OWNING_METHOD_3 = '3'
HOUSE_OWNING_METHOD_4 = '4'
HOUSE_OWNING_METHOD_5 = '5'
HOUSE_OWNING_METHOD_6 = '88'

HOUSE_OWNING_METHOD_CHOICES = (
    (HOUSE_OWNING_METHOD_1, 'Construida com licenca'),
    (HOUSE_OWNING_METHOD_2, 'Construida sem licenca'),
    (HOUSE_OWNING_METHOD_3, 'Comprou do estado ou APIE'),
    (HOUSE_OWNING_METHOD_4, 'Comprou a outro'),
    (HOUSE_OWNING_METHOD_5, 'Adiquiriu por heranca'),
    (HOUSE_OWNING_METHOD_6, 'Outro'),
)

# Pavement type
HOUSE_PAVEMENT_1 = '1'
HOUSE_PAVEMENT_2 = '2'
HOUSE_PAVEMENT_3 = '3'
HOUSE_PAVEMENT_4 = '4'
HOUSE_PAVEMENT_5 = '5'
HOUSE_PAVEMENT_6 = '6'
HOUSE_PAVEMENT_7 = '88'

HOUSE_PAVEMENT_CHOICES = (
    (HOUSE_PAVEMENT_1, 'Madeira/parquet'),
    (HOUSE_PAVEMENT_2, 'Marmore/granito'),
    (HOUSE_PAVEMENT_3, 'Cimento'),
    (HOUSE_PAVEMENT_4, 'Mosaico/tijoleira'),
    (HOUSE_PAVEMENT_5, 'Adobe/terra batida'),
    (HOUSE_PAVEMENT_6, 'Sem nada'),
    (HOUSE_PAVEMENT_7, 'Outro'),
)

# light type choices
LIGHT_TYPE_1 = '1'
LIGHT_TYPE_2 = '2'
LIGHT_TYPE_3 = '3'
LIGHT_TYPE_4 = '4'
LIGHT_TYPE_5 = '5'
LIGHT_TYPE_6 = '6'
LIGHT_TYPE_7 = '7'
LIGHT_TYPE_8 = '8'

LIGHT_TYPE_CHOICES = (
    (LIGHT_TYPE_1, 'Electricidade da rede publica'),
    (LIGHT_TYPE_2, 'Gerador/placa solar'),
    (LIGHT_TYPE_3, 'Petroleo/parafina/querosene'),
    (LIGHT_TYPE_4, 'Velas'),
    (LIGHT_TYPE_5, 'Baterias'),
    (LIGHT_TYPE_6, 'Lenha'),
    (LIGHT_TYPE_7, 'Pilhas'),
    (LIGHT_TYPE_8, 'Outro'),
)

# Sanitation type
SANITATION_TYPE_1 = '1'
SANITATION_TYPE_2 = '2'
SANITATION_TYPE_3 = '3'
SANITATION_TYPE_4 = '4'
SANITATION_TYPE_5 = '5'
SANITATION_TYPE_6 = '6'
SANITATION_TYPE_7 = '7'

SANITATION_TYPE_CHOICES = (
    (SANITATION_TYPE_1, 'Retrete com autolismo dentro de casa'),
    (SANITATION_TYPE_2, 'Retrete com autolismo fora de casa'),
    (SANITATION_TYPE_3, 'Retrete sem autoclismo'),
    (SANITATION_TYPE_4, 'Latrina melhorada'),
    (SANITATION_TYPE_5, 'Latrina tradicional melhorada'),
    (SANITATION_TYPE_6, 'Latrina nao melhorada'),
    (SANITATION_TYPE_7, 'Sem retrete/latrina'),
)

# trash treatment
TRASH_TREATMENT_1 = '1'
TRASH_TREATMENT_2 = '2'
TRASH_TREATMENT_3 = '3'
TRASH_TREATMENT_4 = '4'
TRASH_TREATMENT_5 = '5'
TRASH_TREATMENT_6 = '6'

TRASH_TREATMENT_CHOICES = (
    (TRASH_TREATMENT_1, 'Recolhido pelas autoridades municipais/contentor'),
    (TRASH_TREATMENT_2, 'Recolhido pela empresa privada/associacao'),
    (TRASH_TREATMENT_3, 'Enterra'),
    (TRASH_TREATMENT_4, 'Queima'),
    (TRASH_TREATMENT_5, 'Deita no terreno baldio/pantano/lago/rio/mar'),
    (TRASH_TREATMENT_6, 'Outro'),
)

# Water main
WATER_MAIN_1 = '1'
WATER_MAIN_2 = '2'
WATER_MAIN_3 = '3'
WATER_MAIN_4 = '4'
WATER_MAIN_5 = '5'
WATER_MAIN_6 = '6'
WATER_MAIN_7 = '7'
WATER_MAIN_8 = '8'
WATER_MAIN_9 = '9'
WATER_MAIN_10 = '10'
WATER_MAIN_11 = '11'
WATER_MAIN_12 = '12'
WATER_MAIN_13 = '13'

WATER_MAIN_CHOICES = (
    (WATER_MAIN_1, 'Agua canalizada dentro  da casa'),
    (WATER_MAIN_2, 'Agua canalizada fora de casa/quintal'),
    (WATER_MAIN_3, 'Agua canalizada na casa do vizinho'),
    (WATER_MAIN_4, 'Agua do fontan·rio publico'),
    (WATER_MAIN_5, 'Agua do  furo poco protegido com bomba manual'),
    (WATER_MAIN_6, 'Agua do poÁo protegido sem bomba'),
    (WATER_MAIN_7, 'Agua do poco nao protegido'),
    (WATER_MAIN_8, 'Agua de nascente'),
    (WATER_MAIN_9, 'Agua de superficie (rio, lago, lagoa)'),
    (WATER_MAIN_10, 'Agua da chuva'),
    (WATER_MAIN_11, 'Agua dde tanques camioes'),
    (WATER_MAIN_12, 'Agua mineral'),
    (WATER_MAIN_13, 'Outra'),
)

# HIV Test Periods choices
LESS_THREE_MONTHS = '1'
MORE_THREE_MONTHS = '2'

LAST_TEST_PERIOD_COICES = ((LESS_THREE_MONTHS, 'Menos de 3 meses atrás'),
                           (MORE_THREE_MONTHS, 'Mais de 3 meses atrás'))

# HIV results choices
POSITIVE_HIV = '1'
NEGATIVE_HIV = '2'
UNKNOWN_HIV = '3'
DKNOW_HIV_STATUS = '99'

HIV_RESULT_CHOICES = ((POSITIVE_HIV, 'Positivo'),
                      (NEGATIVE_HIV, 'Negativo'),
                      (UNKNOWN_HIV, 'Indeterminado'),
                      (DKNOW_HIV_STATUS, 'Não sabe/Não'),
                      )

# TARV AND TPI MONTHS
MONTH_1 = '1'
MONTH_2 = '2'
MONTH_3 = '3'
MONTH_4 = '4'
MONTH_5 = '5'
MONTH_6 = '6'
MONTH_7 = '7'
MONTH_8 = '8'
MONTH_9 = '9'
MONTH_10 = '10'
MONTH_11 = '11'
MONTH_12 = '12'

MONTH_CHOICES = (
    (MONTH_1, 'Janeiro'),
    (MONTH_2, 'Fevereiro'),
    (MONTH_3, 'Março'),
    (MONTH_4, 'Abril'),
    (MONTH_5, 'Maio'),
    (MONTH_6, 'Junho'),
    (MONTH_7, 'Julho'),
    (MONTH_8, 'Agosto'),
    (MONTH_9, 'Setembro'),
    (MONTH_10, 'Outubro'),
    (MONTH_11, 'Novembro'),
    (MONTH_12, 'Dezembro'),
)

NOT_CONSENTED_1 = '1'
NOT_CONSENTED_2 = '2'
NOT_CONSENTED_3 = '3'
NOT_CONSENTED_4 = '4'

NOT_CONSENTED_REASON_CHOICES = (
    (NOT_CONSENTED_1, 'Testou negativo nos últimos 12 meses'),
    (NOT_CONSENTED_2, 'Não quer saber'),
    (NOT_CONSENTED_3, 'Não tem permissão'),
    (NOT_CONSENTED_4, 'Outra razão, especificar'),
)

# Result of the interview
COMPLETE = '1'
INCOMPLETE = '2'
ABSENT_HH = '3'
REFUSAL_DURING = '4'
TOTAL_REFUSAL = '5'
POSTPONED_INTERVIEW = '6'
HOUSE_VACANT = '7'
HOUSE_DEMOLISHED = '8'
HOUSE_NOT_FOUND = '9'
OTHER = '88'

VISIT_RESULT_CHOICES = (
    (COMPLETE, 'Completo'),
    (INCOMPLETE, 'Incompleto'),
    (ABSENT_HH, 'Todo Agregado Familiar Ausente'),
    (REFUSAL_DURING, 'Recusa Durante A Entrevista'),
    (TOTAL_REFUSAL, 'Recusa Total'),
    (POSTPONED_INTERVIEW, 'Entrevista Adiada'),
    (HOUSE_VACANT, 'Casa Vaga'),
    (HOUSE_DEMOLISHED, 'Casa Demolida/destruída'),
    (HOUSE_NOT_FOUND, 'Casa Não Encontrada'),
    (OTHER, 'Outros'),
)

# Local choices
PUBLIC_HU = '1'
PRIVATE_HU = '2'
PHARMACY = '3'
VOLUNTEER = '4'
TRADITIONAL_MEDICINE = '5'
OTHER = '88'

TREATMENT_LOCAL_CHOICES = (
    (PUBLIC_HU, 'US Publica'),
    (PRIVATE_HU, 'US Privada'),
    (PHARMACY, 'Farmácia'),
    (VOLUNTEER, 'Voluntário/ape'),
    (TRADITIONAL_MEDICINE, 'Praticante De Medicina Tradicional'),
    (OTHER, 'Outro'),
)

# Local that visited
PUBLIC_HU_VISIT = '1'
PRIVATE_HU_VISIT = '2'
PHARMACY_VISIT = '3'
INFORMAL_MARKET_VISIT = '4'
VOLUNTEER_VISIT = '5'
TRADITIONAL_MEDICINE_VISIT = '6'
CHURCH_VISIT = '7'
OTHER_VISIT = '88'

VISIT_HU_LOCAL_CHOICES = (
    (PUBLIC_HU_VISIT, 'US Pública'),
    (PRIVATE_HU_VISIT, 'US Privada'),
    (PHARMACY_VISIT, 'Farmácia'),
    (INFORMAL_MARKET_VISIT, 'Mercado Informal'),
    (VOLUNTEER_VISIT, 'Voluntário ou ape'),
    (TRADITIONAL_MEDICINE_VISIT, 'Praticante De Medicina Tradicional'),
    (CHURCH_VISIT, 'Igreja'),
    (OTHER_VISIT, 'Outro'),
)

# Reason for not visiting HU
REASON_DISTANCE = '1'
REASON_FINANCIAL = '2'
REASON_TIME = '3'
REASON_BAD_EXP = '4'
REASON_LOSE_JOB = '5'
REASON_OTHER = '88'

REASON_NOT_VISITING_HU_CHOICES = (
    (REASON_DISTANCE, 'Distância longa à unidade de saúde'),
    (REASON_FINANCIAL, 'Falta de dinheiro para transporte, testes, remédios'),
    (REASON_TIME, 'Falta de tempo'),
    (REASON_BAD_EXP, 'Tenho más experienciais com o hospital'),
    (REASON_LOSE_JOB, 'Medo de perder o trabalho'),
    (REASON_OTHER, 'Outro')

)

# Genexpert stages
STAGE_1 = '1'
STAGE_2 = '2'
STAGE_3 = '3'

GENXEPERT_STAGES_CHOICES = (
    (STAGE_1, 'Amostra 1'),
    (STAGE_2, 'Amostra 2'),
    (STAGE_3, 'Amostra 3')
)

# Genepert status
POSITIVE_GEN = '1'
NEGATIVE_GEN = '2'
ERROR_GEN = '3'
INVALID = '4'
INDETERMINATE = '5'
NO_RESULT = '6'
TRACE = '7'

GENEXPERT_CHOICES = (
    (POSITIVE_GEN, 'Positivo'),
    (NEGATIVE_GEN, 'Negativo'),
    (ERROR_GEN, 'Erro'),
    (INVALID, 'Invalido'),
    (INDETERMINATE, 'Indeterminado'),
    (NO_RESULT, 'Sem resultado'),
    (TRACE, 'Vestígio')
)

# Positivity status_GENEXPERT
POSITIVITY_HIGH = '1'
POSITIVITY_MEDIUM = '2'
POSITIVITY_LOW = '3'
POSITIVITY_VERY_LOW = '4'

POSITIVITY_CHOICES = (
    (POSITIVITY_HIGH, 'Alto'),
    (POSITIVITY_MEDIUM, 'Médio'),
    (POSITIVITY_LOW, 'Baixo'),
    (POSITIVITY_VERY_LOW, 'Muito baixo')
)

# Rifampicina
RIFAMPICINA_1 = '1'
RIFAMPICINA_2 = '2'
RIFAMPICINA_3 = '3'
RIFAMPICINA_4 = '4'
RIFAMPICINA_5 = '5'
RIFAMPICINA_6 = '6'
RIFAMPICINA_7 = '7'

RIFAMPICINA_CHOICES = (
    (RIFAMPICINA_1, 'Sensivel'),
    (RIFAMPICINA_2, 'Resistente'),
    (RIFAMPICINA_3, 'Indeterminado'),
    (RIFAMPICINA_4, 'Invalido'),
    (RIFAMPICINA_5, 'Erro'),
    (RIFAMPICINA_6, 'Sem resultado'),
    (RIFAMPICINA_7, 'Vestígio')
)

# Erros

ERROR_1 = '1'
ERROR_2 = '2'
ERROR_3 = '3'
ERROR_4 = '4'
ERROR_5 = '5'
ERROR_6 = '6'

ERROR_GEN_CHOICES = (
    (ERROR_1, 'Erro 5006/5007/5008'),
    (ERROR_2, 'Erro 5011'),
    (ERROR_3, 'Erro 2008'),
    (ERROR_4, 'Erro 2127'),
    (ERROR_5, 'Erro 2037'),
    (ERROR_6, 'Erro 2014/3074/3075/1011('),
)

# GX error types
REPEATED_RESULT = '1'
INVALID_SAMPLE_ID = '2'

GX_ERROR_TYPES = (
    (REPEATED_RESULT, 'Resultado repetido'),
    (INVALID_SAMPLE_ID, 'Amostra Invalida'),
)

YESNOXRAY_CHOICES = (
    (YES, 'Sim'),
    (NO, 'Não'),
    (XRAYDENIED, 'Recusou//Não Feito'),
)

XRAY_NOTDONE_REASON_1 = '1'
XRAY_NOTDONE_REASON_2 = '5'
XRAY_NOTDONE_REASON_3 = '7'
XRAY_NOTDONE_REASON_4 = '88'

XRAY_NOTDONE_REASON_CHOICES = (
    (XRAY_NOTDONE_REASON_1, 'Avaria do equipamento de RX '),
    (XRAY_NOTDONE_REASON_2, 'Recusou por ser mulher grávida'),
    (XRAY_NOTDONE_REASON_3, 'Recusou por não poder estar de pe'),
    (XRAY_NOTDONE_REASON_4, 'Outro')
)

XRAY_RESULTS_1 = '0'
XRAY_RESULTS_2 = '1'
XRAY_RESULTS_3 = '2'
XRAY_RESULTS_4 = '7'

XRAY_RESULT_CHOICES = (
    (XRAY_RESULTS_1, 'Normal'),
    (XRAY_RESULTS_2, 'Abnormal (Sugestivo de TB pulmonar)'),
    (XRAY_RESULTS_3, 'Outra anormalidade'),
    (XRAY_RESULTS_4, 'Não interpretável (Repetir Rx, maximo 3X)'),
)

SPUTUM_PROD_MODE_1 = '1'
SPUTUM_PROD_MODE_2 = '2'

SPUTUM_PROD_MODE_CHOICES = (
    (SPUTUM_PROD_MODE_1, 'Produzida espontaneamente'),
    (SPUTUM_PROD_MODE_2, 'Produzida depois da fisioterapia respiratoria'),
)

SAMPLE_SALIVA = '1'
SAMPLE_MUCOIDE = '2'
SAMPLE_MUCOPULENTA = '3'
SAMPLE_HEMOPTOICA = '4'

SPUTUM_SAMPLE_CHOICES = (
    (SAMPLE_SALIVA, 'Saliva'),
    (SAMPLE_MUCOIDE, 'Mucoíde'),
    (SAMPLE_MUCOPULENTA, 'Mucupulenta'),
    (SAMPLE_HEMOPTOICA, 'Hemoptóica'),
)

RISKFACTOR_MEDIA_1 = '1'
RISKFACTOR_MEDIA_2 = '2'
RISKFACTOR_MEDIA_3 = '3'

RISKFACTOR_MEDIA_CHOICES = (
    (RISKFACTOR_MEDIA_1, 'Uma ou mais vezes por semana'),
    (RISKFACTOR_MEDIA_2, 'menos de uma vez por semana'),
    (RISKFACTOR_MEDIA_3, 'Não lê'),

)

WORKPLACE_OPEN = '1'
WORKPLACE_CLOSED = '2'
WORKPLACE_BOTH = '3'

WORKPLACE_SPACE_CHOICES = (
    (WORKPLACE_OPEN, 'Aberto'),
    (WORKPLACE_CLOSED, 'Fechado'),
    (WORKPLACE_BOTH, 'Ambos'),
)

FARMER = '1'
PEASANT = '2'
FISHERMAN = '3'
MERCHANT = '4'
MINER = '5'
STATE = '6'
RETIRED = '7'
STUDENT = '8'
DOMESTIC = '9'
UNEMPLOYED = '10'
OTHER = '88'

OCCUPATION_CHOICES = (
    (FARMER, 'Agricultor'),
    (PEASANT, 'Camponês'),
    (FISHERMAN, 'Pescador'),
    (MERCHANT, 'Comerciante/vendedor'),
    (MINER, 'Mineiro'),
    (STATE, 'Funcionário administrativo do Estado/do sector privado'),
    (RETIRED, 'Reformado'),
    (STUDENT, 'Estudante'),
    (DOMESTIC, 'Doméstico'),
    (UNEMPLOYED, 'Desempregado'),
    (OTHER, 'Outro'),
)

MINE_TYPE_1 = '1'
MINE_TYPE_2 = '2'
MINE_TYPE_3 = '3'

MINE_TYPE_CHOICES = (
    (MINE_TYPE_1, 'Mina a ceu aberto'),
    (MINE_TYPE_2, 'Mina subterranea'),
    (MINE_TYPE_3, 'Ambas'),
)

SMOKE_DAILY = '1'
SMOKE_WEEKLY = '2'
SMOKE_MONTHLY = '3'
SMOKE_lESS_WEEKLY = '4'
SMOKE_NEVER = '5'
SMOKE_DONTKNOW = '6'

SMOKING_FREQUENCY_CHOICES = (
    (SMOKE_DAILY, 'Diariamente'),
    (SMOKE_WEEKLY, 'Semanalmente'),
    (SMOKE_MONTHLY, 'Mensalmente'),
    (SMOKE_lESS_WEEKLY, 'Menos que mensalmente'),
    (SMOKE_NEVER, 'Nunca'),
    (SMOKE_DONTKNOW, 'Não sabe'),
)

DRINKING_AMOUNT_1 = '0'
DRINKING_AMOUNT_2 = '1'
DRINKING_AMOUNT_3 = '2'
DRINKING_AMOUNT_4 = '3'
DRINKING_AMOUNT_5 = '4'

DRINKING_AMOUNT_CHOICES = (
    (DRINKING_AMOUNT_1, '1 ou 2'),
    (DRINKING_AMOUNT_2, '3 ou 4'),
    (DRINKING_AMOUNT_3, '5 ou 6'),
    (DRINKING_AMOUNT_4, '7 a 9'),
    (DRINKING_AMOUNT_5, '10 ou mais'),
)

DRINKING_FREQUENCY_0 = '0'
DRINKING_FREQUENCY_1 = '1'
DRINKING_FREQUENCY_2 = '2'
DRINKING_FREQUENCY_3 = '3'
DRINKING_FREQUENCY_4 = '4'

DRINKING_FREQUENCY_CHOICES = (
    (DRINKING_FREQUENCY_0, 'Nunca'),
    (DRINKING_FREQUENCY_1, 'Uma vez por mês ou menos'),
    (DRINKING_FREQUENCY_2, '2 a 4 vezes por mês'),
    (DRINKING_FREQUENCY_3, '2 a 3 vezes por semana'),
    (DRINKING_FREQUENCY_4, '4 ou mais vezes por semana'),
)

FULLY_AGREE = '1'
AGREE = '2'
DONTKNOW_TB = '3'
DISAGREE = '4'
FULLY_DISAGREE = '5'

COMMUNITY_TB_CHOICES = (
    (FULLY_AGREE, 'Concorda plenamente'),
    (AGREE, 'Concorda'),
    (DONTKNOW_TB, 'Nao sabe'),
    (DISAGREE, 'Discorda'),
    (FULLY_DISAGREE, 'Discorda plenamente'),
)

# Cluster status
INACTIVE = '0'
OPEN = '1'
CLOSED = '2'

CLUSTER_STATUS_CHOICES = (
    (INACTIVE, 'Inactivo'),
    (OPEN, 'Aberto'),
    (CLOSED, 'Fechado'),
)

GENOTYPE_CM_1 = '1'
GENOTYPE_CM_2 = '2'
GENOTYPE_CM_3 = '3'
GENOTYPE_CM_4 = '4'
GENOTYPE_CM_5 = '5'
GENOTYPE_CM_6 = '6'
GENOTYPE_CM_7 = '7'
GENOTYPE_CM_8 = '8'
GENOTYPE_CM_9 = '9'
GENOTYPE_CM_10 = '10'
GENOTYPE_CM_11 = '11'
GENOTYPE_CM_12 = '12'
GENOTYPE_CM_13 = '13'
GENOTYPE_CM_14 = '14'
GENOTYPE_CM_15 = '15'
GENOTYPE_CM_16 = '16'

GENOTYPE_CM_CHOICES = (
    (GENOTYPE_CM_1, 'Mycobacterium spec'),
    (GENOTYPE_CM_2, 'M.avium'),
    (GENOTYPE_CM_3, 'M.cehlonae'),
    (GENOTYPE_CM_4, 'M.abscessus'),
    (GENOTYPE_CM_5, 'M.fortuitum'),
    (GENOTYPE_CM_6, 'M.gordonae'),
    (GENOTYPE_CM_7, 'M.intracellulare'),
    (GENOTYPE_CM_8, 'M.scrofulaceum'),
    (GENOTYPE_CM_9, 'M.interjectum'),
    (GENOTYPE_CM_10, 'M.kansasii'),
    (GENOTYPE_CM_11, 'M.malmoense'),
    (GENOTYPE_CM_12, 'M.marinum/M.ulcerans'),
    (GENOTYPE_CM_13, 'M.tuberculosis complex'),
    (GENOTYPE_CM_14, 'M.peregrinum'),
    (GENOTYPE_CM_15, 'M.xenopi'),
    (GENOTYPE_CM_16, 'Invalido'),
)

GENOTYPE_MT_1 = '1'
GENOTYPE_MT_2 = '2'
GENOTYPE_MT_3 = '3'
GENOTYPE_MT_4 = '4'

GENOTYPE_MT_CHOICES = (
    (GENOTYPE_MT_1, 'H e R - Sensivel'),
    (GENOTYPE_MT_2, 'H e R - Resistente'),
    (GENOTYPE_MT_3, 'H - Sensivel e R- Resistente'),
    (GENOTYPE_MT_4, 'H - Resistente e R-Sensivel'),
)

BACILOSCOPY_1 = '1'
BACILOSCOPY_2 = '2'
BACILOSCOPY_3 = '3'
BACILOSCOPY_4 = '4'
BACILOSCOPY_5 = '5'

BACILOSCOPY_CHOICES = (
    (BACILOSCOPY_1, 'Contagem exacta'),
    (BACILOSCOPY_2, '+'),
    (BACILOSCOPY_3, '++'),
    (BACILOSCOPY_4, '+++'),
    (BACILOSCOPY_5, 'Negativo'),
)

ZIEHL_1 = '1'
ZIEHL_2 = '2'
ZIEHL_3 = '3'

ZIEHL_CHOICES = (
    (ZIEHL_1, 'BAAR Positivo'),
    (ZIEHL_2, 'BAAR Negativo'),
    (ZIEHL_3, 'Mix'),
)

REJECTION_CRL_1 = '1'
REJECTION_CRL_2 = '2'
REJECTION_CRL_3 = '3'
REJECTION_CRL_4 = '4'

REJECTION_CRL_CHOICES = (
    (REJECTION_CRL_1, 'Amostra derramada'),
    (REJECTION_CRL_2, 'Recipiente vazio'),
    (REJECTION_CRL_3, 'Amostra nao identificada'),
    (REJECTION_CRL_4, 'Tubo quebrado'),
)

PROBLEMATIC_SAMPLE_1 = '1'
PROBLEMATIC_SAMPLE_2 = '2'

PROBLEMATIC_SAMPLE_CHOICES = (
    (PROBLEMATIC_SAMPLE_1, 'Volume insuficiente - abaixo de 1ml'),
    (PROBLEMATIC_SAMPLE_2, 'Nao consta no formulario de transporte de amostras'),
)

CULTURE_RESULT_1 = '1'
CULTURE_RESULT_2 = '2'
CULTURE_RESULT_3 = '3'

CULTURE_RESULT_CHOICES = (
    (CULTURE_RESULT_1, 'Positivo'),
    (CULTURE_RESULT_2, 'Negativo'),
    (CULTURE_RESULT_3, 'Contaminada'),
)

POSITIVITY_WEEK_1 = '1'
POSITIVITY_WEEK_2 = '2'
POSITIVITY_WEEK_3 = '3'
POSITIVITY_WEEK_4 = '4'
POSITIVITY_WEEK_5 = '5'
POSITIVITY_WEEK_6 = '6'
POSITIVITY_WEEK_7 = '7'
POSITIVITY_WEEK_8 = '8'
POSITIVITY_WEEK_9 = '9'
POSITIVITY_WEEK_10 = '10'

POSITIVITY_WEEK_CHOICES = (
    (POSITIVITY_WEEK_1, '24 Horas'),
    (POSITIVITY_WEEK_2, '48 Horas'),
    (POSITIVITY_WEEK_3, '1ª Semana'),
    (POSITIVITY_WEEK_4, '2ª Semana'),
    (POSITIVITY_WEEK_5, '3ª Semana'),
    (POSITIVITY_WEEK_6, '4ª Semana'),
    (POSITIVITY_WEEK_7, '5ª Semana'),
    (POSITIVITY_WEEK_8, '6ª Semana'),
    (POSITIVITY_WEEK_9, '7ª Semana'),
    (POSITIVITY_WEEK_10, '8ª Semana'),
)

GRADATION_1 = '1'
GRADATION_2 = '2'
GRADATION_3 = '3'
GRADATION_4 = '4'

GRADATION_CHOICES = (
    (GRADATION_1, 'Contagem exata'),
    (GRADATION_2, '+'),
    (GRADATION_3, '++'),
    (GRADATION_4, '+++'),
)

CULTURE_TUBES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)

BLOOD_RESULT_1 = '1'
BLOOD_RESULT_2 = '2'

BLOOD_RESULT_CHOICES = (
    (BLOOD_RESULT_1, 'Positivo'),
    (BLOOD_RESULT_2, 'Negativo'),
)

TSA_RESULT_1 = '1'
TSA_RESULT_2 = '2'

TSA_RESULT_CHOICES = (
    (TSA_RESULT_1, 'Sensivel'),
    (TSA_RESULT_2, 'Resistente'),
)

TSA_RESISTENT_1_1 = '1'
TSA_RESISTENT_1_2 = '2'
TSA_RESISTENT_1_3 = '3'
TSA_RESISTENT_1_4 = '4'
TSA_RESISTENT_1_5 = '5'

TSA_RESISTENT_1_CHOICES = (
    (TSA_RESISTENT_1_1, 'H'),
    (TSA_RESISTENT_1_2, 'R'),
    (TSA_RESISTENT_1_3, 'Z'),
    (TSA_RESISTENT_1_4, 'E'),
    (TSA_RESISTENT_1_5, 'S'),
)

TSA_RESISTENT_2_1 = '1'
TSA_RESISTENT_2_2 = '2'
TSA_RESISTENT_2_3 = '3'
TSA_RESISTENT_2_4 = '4'
TSA_RESISTENT_2_5 = '5'

TSA_RESISTENT_2_CHOICES = (
    (TSA_RESISTENT_2_1, 'H'),
    (TSA_RESISTENT_2_2, 'R'),
    (TSA_RESISTENT_2_3, 'Z'),
    (TSA_RESISTENT_2_4, 'E'),
    (TSA_RESISTENT_2_5, 'S'),
)

CXR_READING_SIDES_CHOICES = (
    ('1', 'Esquerda'),
    ('2', 'Direita'),
    ('3', 'Bilateral'),
    ('4', 'Nao existe'),
)

CXR_FINAL_READING_SIDES_CHOICES = (
    ('1', "Detectado Anormal - Nao significante"),
    ('2', "Detectado Anormal - significante - Sem doenca activa"),
    ('3', "Detectado anormal - significante - nao tuberculose"),
    ('4', "Detectado anormal - significante - tuberculose"),
    ('5', "Detectado anormal, significate - nao classificado")
)

CXR_READING_STATUS_CHOICES = (
    ('1','Pendente - Requer Discussão'),
    ('2','Completa'),
)

WORKSHEET_TYPE_1 = '1'
WORKSHEET_TYPE_2 = '2'
WORKSHEET_TYPE_3 = '3'
WORKSHEET_TYPE_4 = '4'
WORKSHEET_TYPE_5 = '5'
WORKSHEET_TYPE_6 = '6'

WORKSHEET_TYPE = (
    (WORKSHEET_TYPE_1, 'Ziehl-Neelsen (ZN)'),
    (WORKSHEET_TYPE_2, 'Genotype CM'),
    (WORKSHEET_TYPE_3, 'Genotype MTBDRplus'),
    (WORKSHEET_TYPE_4, 'GeneXpert'),
    (WORKSHEET_TYPE_5, 'TSA'),
    (WORKSHEET_TYPE_6, 'Culture'),
)

STAGES = (
    ('F01', 'Census'),
    ('F02', 'Reception'),
    ('F03', 'Symptoms Screening'),
    ('F04', 'Risk Factors'),
    ('F05', 'Xray Capture'),
    ('F051', 'Xray Reading'),
    ('F06', 'Sputum Collection'),
    ('F08', 'HIV Screening'),
    ('F10', 'Reimbursment'),
)