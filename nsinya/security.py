CENSUS_TEAM = "Census Team"
FIELD_TEAM = "Field Team"
CENTRAL_LAB_TEAM = "Central Lab team"
CENTRAL_RADIOLOGY = "Central Radiology"
LAB_ADMINS = "Lab_Admins"

CENSUS_GROUPS = []
CRL_LAB_GROUPS = [LAB_ADMINS, 'Lab_Tecs']
DIAGNOSIS_GROUPS = []
CONFIG_GROUPS = ['Field_IT']


def is_in_groups(request, groups):
    if request.user.is_superuser:
        return True

    if not len(groups):
        return False

    for group in groups:
        if request.user.groups.filter(name=group).exists():
            return True

    return False
