from django.conf.urls import include, url
from .views import *

app_name = 'nsinya'

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^api/', include('nsinya.api.urls')),
    url(r'^census/eligibility/$', CheckEligibilityView.as_view(), name="check-eligibility"),
    url(r'^census/eligibility/(?P<pin>[\w{}.-]+)/$', CheckEligibilityView.as_view(), name="check-eligibility"),
    url(r'^census/participants/$', ParticipantListingView.as_view(), name="participant-listing"),
    url(r'^census/participant/(?P<pin>[\w{}.-]+)/$', ParticipantDashboardView.as_view(), name="participant-dashboard"),
    url(r'^census/households/$', HouseholdListingView.as_view(), name="household-listing"),
    url(r'^census/household/(?P<household_id>[\w{}.-]+)/$', HouseholdDashboardView.as_view(), name="household-dashboard"),
    url(r'^diagnosis/sampletracking/$', SampleTrackingView.as_view(), name="sample-tracking"),
    url(r'^diagnosis/sample/(?P<sample_id>.+)/$', SampleDashboardView.as_view(), name="sample-tracking-dashboard"),
    url(r'^diagnosis/medical_review_panel/$', MedicalReviewPanelView.as_view(), name="medical-review_panel"),
    url(r'^diagnosis/review_participant/(?P<pin>[\w{}.-]+)/$', ReviewParticipantView.as_view(), name="review-participant"),
    url(r'^radiology/central_cxr_reading_list/$', CentralCXRReadingList.as_view(), name="central-cxr-reading-list"),
    url(r'^radiology/central_cxr_reading/(?P<pin>[\w{}.-]+)/$', CentralCXRReading.as_view(), name="read-participant-cxr"),
    url(r'^crl/reception/$', BatchReceptionView.as_view(), name="batch-reception"),
    url(r'^crl/receive/batch/(?P<batch_id>[0-9]+)/$', ReceiveBatchView.as_view(), name="receive-batch"),
    url(r'^crl/receive/sample/(?P<batch_id>[0-9]+)/$', ReceiveSampleView.as_view(), name="receive-sample"),
    url(r'^crl/receive/sample/(?P<batch_id>[0-9]+)/(?P<sample_id>\w+)/$', ReceiveSampleView.as_view(), name="receive-sample"),
    url(r'^crl/orphan_sample/$', ReceiveOrphanSampleView.as_view(), name="receive-orphan-sample"),
    url(r'^crl/orphan_sample/(?P<sample_id>\w+)/$', ReceiveOrphanSampleView.as_view(), name="receive-orphan-sample"),
    url(r'^crl/culture/$', CultureListingView.as_view(), name="culture-listing"),
    url(r'^crl/culture_reading/$', CultureReadingView.as_view(), name="culture-reading"),
    url(r'^crl/culture_reading/(?P<sample_id>\w+)/$', CultureReadingView.as_view(), name="culture-reading"),
    url(r'^crl/culture_edit/(?P<culture_id>\w+)/$', CultureEditView.as_view(), name="culture-edit"),
    url(r'^crl/culture_validation/$', CultureValidationView.as_view(), name="culture-validation"),
    url(r'^crl/culture_validation/(?P<sample_id>.+)/$', CultureValidationView.as_view(), name="culture-validation"),
    url(r'^crl/culture/validate/(?P<culture_id>.+)/$', ValidateCultureView.as_view(), name="validate-culture"),
    url(r'^crl/cultures/validate/$', ValidateCulturesView.as_view(), name="validate-cultures"),
    url(r'^crl/cultures/validate/(?P<sample_id>\w+)/$', ValidateCulturesView.as_view(), name="validate-cultures"),
    url(r'^crl/tsa/$', TSAListingView.as_view(), name="tsa-listing"),
    url(r'^crl/tsa/create/$', TSACreateView.as_view(), name="create-tsa"),
    url(r'^crl/tsa/create/(?P<sample_id>\w+)/$', TSACreateView.as_view(), name="create-tsa"),
    url(r'^crl/tsa_validation/$', TSAValidationView.as_view(), name="tsa-validation"),
    url(r'^crl/smear/$', SmearListingView.as_view(), name="smear-listing"),
    url(r'^crl/smear/(?P<sample_id>\w+)/$', SmearListingView.as_view(), name="smear-listing"),
    url(r'^crl/smear_validation/$', ValidateSmearView.as_view(), name="validate-smear"),
    url(r'^crl/smear_validation/(?P<sample_id>.+)/$', ValidateSmearView.as_view(), name="validate-smear"),
    url(r'^crl/worksheet/$', WorksheetView.as_view(), name="worksheet-creation"),
    url(r'^crl/worksheet/(?P<sample_id>\w+)/$', WorksheetView.as_view(), name="worksheet-creation"),
    url(r'^crl/worksheet/(?P<sample_id>\w+)/(?P<remove>\w+)$', WorksheetView.as_view(), name="worksheet-creation"),
    url(r'^crl/worksheet_select_type/(?P<sample_id>\w+)/(?P<type>\w+)$', WorksheetView.as_view(), name="worksheet-creation"),
    url(r'^crl/worksheet_select_type/(?P<type>\w+)/$', WorksheetView.as_view(), name="worksheet-creation"),
    url(r'^crl/worksheet_details/(?P<worksheet_code>[\w{}.-]+)/$', WorksheetDetailsView.as_view(), name="worksheet-details"),
    url(r'^crl/worksheetlist/$', WorksheetListView.as_view(), name="worksheet-list"),
    url(r'^crl/worksheetlist/(?P<worksheet_id>\w+)/$', WorksheetListView.as_view(), name="worksheet-list"),
    url(r'^crl/worksheetedit/(?P<worksheet_id>\w+)/$', WorksheetEditView.as_view(), name="worksheet-edit"),
    url(r'^crl/worksheetedit/(?P<sample_id>\w+)/(?P<worksheet_id>\w+)/(?P<remove>\w+)$', WorksheetEditView.as_view(), name="worksheet-edit-list"),
    url(r'^crl/inoculation/$', WorksheetInoculationView.as_view(), name="worksheet-inoculation"),
    url(r'^crl/inoculation/(?P<worksheet_id>\w+)/$', WorksheetInoculationView.as_view(), name="worksheet-inoculation"),
    url(r'^diagnosis/genexpert/$', GeneXpertListingView.as_view(), name="genexpert-listing"),
    url(r'^config/clusters/$', ClusterActivationView.as_view(), name="cluster-activation"),
    url(r'^census/cluster/(?P<cluster_id>[0-9]+)/$', ClusterBatchesView.as_view(), name="cluster-batches"),
    url(r'^census/cluster/(?P<cluster_code>[0-9]+)/status/(?P<status>[0-9]+)/$', ClusterStatusUpdateView.as_view(), name="cluster-status-update"),
    url(r'^crl/genotype_cm/$', Genotype_CMListingView.as_view(), name="genotype_cm-listing"),
    url(r'^crl/genotype_cm/(?P<sample_id>\w+)/$', Genotype_CMListingView.as_view(), name="genotype_cm-listing"),
    url(r'^crl/genotype_cm_validation/$', ValidateGenotypeCmView.as_view(), name="validate-genotype_cm"),
    url(r'^crl/genotype_cm_validation/(?P<sample_id>.+)/$', ValidateGenotypeCmView.as_view(), name="validate-genotype_cm"),
    url(r'^crl/genotype_mt/$', Genotype_MTListingView.as_view(), name="genotype_mt-listing"),
    url(r'^crl/genotype_mt/(?P<sample_id>\w+)/$', Genotype_MTListingView.as_view(), name="genotype_mt-listing"),
    url(r'^crl/genotype_mt_validation/$', ValidateGenotypeMtView.as_view(), name="validate-genotype_mt"),
    url(r'^crl/genotype_mt_validation/(?P<sample_id>.+)/$', ValidateGenotypeMtView.as_view(), name="validate-genotype_mt"),
    url(r'^crl/ziehl/$', ZiehlListingView.as_view(), name="ziehl-listing"),
    url(r'^crl/ziehl/(?P<sample_id>\w+)/$', ZiehlListingView.as_view(), name="ziehl-listing"),
    url(r'^crl/ziehl_validation/$', ValidateZiehlView.as_view(), name="validate-ziehl"),
    url(r'^crl/ziehl_validation/(?P<sample_id>.+)/$', ValidateZiehlView.as_view(), name="validate-ziehl"),
]