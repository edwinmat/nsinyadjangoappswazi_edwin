from .models import Census, Participant, Collision_log


def fix_census_collisions():
    households = Census.objects.filter(status='NOK')
    if households:
        try:
            qs = Collision_log.objects.latest()
            sequence = qs.sequence
        except Collision_log.DoesNotExist:
            sequence = 800

        for hh in households:
            participants = Participant.objects.filter(census=hh)
            sequence += 1
            old_household = hh.__str__()
            hh.household_number = str(sequence)
            hh.status = 'OK'
            hh.save()
            new_household = hh.__str__()

            for p in participants:
                old_pin = p.pin
                new_pin = new_household + old_pin[5:]
                p.pin=new_pin
                p.save()
                Collision_log(sequence=sequence, old_household=old_household, new_household=new_household, old_pin=old_pin,
                              new_pin=new_pin).save()
