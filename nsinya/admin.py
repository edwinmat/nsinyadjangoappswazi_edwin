from django.contrib import admin
from .models import *


class CensusAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = (
        'cluster_code', 'household_number', 'visit_number', 'visit_date', 'visit_result_start', 'visit_result_end')


class ParticipantAdmin(admin.ModelAdmin):
    search_fields = ('pin', 'name', 'surname',)
    readonly_fields = ('pin',)
    list_display = ('pin', 'surname', 'name', 'gender', 'is_elegible_age')


class SymptomScreeningAdmin(admin.ModelAdmin):
    search_fields = ('participant__pin',)
    list_display = ('__str__', 'survey_date', 'visit_result', 'symptom_eligible', 'illness_realise', 'health_care_need')


class RiskFactorAdmin(admin.ModelAdmin):
    search_fields = ('participant__pin',)
    list_display = (
        '__str__', 'work_mine_history', 'work_prison_history', 'work_healthcare_history',
        'smoking_status', 'drinking_status')


class XRayAdmin(admin.ModelAdmin):
    search_fields = ('participant__pin',)
    list_display = ('__str__', 'xray_status', 'xray_results', 'xray_date', 'xray_time', 'CAD4TB_score')


class SputumCollectionAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = ('sample_id', 'date_sample', 'techinician_name', 'prod_mode', 'amount_sample', 'aspect_sample')


class HIVScreeningAdmin(admin.ModelAdmin):
    search_fields = ('participant__pin',)
    list_display = (
        '__str__', 'last_test_done', 'what_is_last_result', 'if_hiv_positive', 'final_result', 'hiv_status_datetime')


class ReceptionInAdmin(admin.ModelAdmin):
    search_fields = ('participant__pin',)
    list_display = ('__str__', 'date_field', 'consent_participation', 'consent_participation_age', 'participant_age',
                    'participant_gender')


class GeneXpertAdmin(admin.ModelAdmin):
    search_fields = ('patientId',)
    list_display = ('patientId', 'sampleId', 'exportedDate', 'resultText', 'mtb', 'rif', 'positivity')


class GxErrorLogAdmin(admin.ModelAdmin):
    search_fields = ('error_type',)
    list_display = ('error_type', 'creation_date')


class ReimbursmentAdmin(admin.ModelAdmin):
    search_fields = ('partcipant',)
    list_display = ('__str__', 'date_end_field', 'technician_name', 'us_reference', 'transport_reimbursement', 'amount')


class ClusterAdmin(admin.ModelAdmin):
    search_fields = ('code',)
    list_display = ('code', 'status', 'open_date', 'close_date')


class BatchTransportAdmin(admin.ModelAdmin):
    search_fields = ('batch_id',)
    list_display = ('batch_id', 'packaging_date', 'total_sent_samples', 'send_date', 'temperature', 'handover_date',
                    'carrier_reception_date')


class BatchSampleAdmin(admin.ModelAdmin):
    search_fields = ('batch_id',)


class BatchReceptionAdmin(admin.ModelAdmin):
    search_fields = ('batch_id',)
    list_display = ('batch_id', 'total_received_samples', 'temperature', 'received_by', 'reception_date')


class GenotypeCMAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = ('sample_id', 'result', 'result_date', 'technician_name', 'validated_by', 'validation_date')


class GenotypeMTBDRAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = ('sample_id', 'result', 'result_date', 'technician_name', 'validated_by', 'validation_date')


class SmearAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = ('sample_id', 'bacil', 'result_date', 'result_time', 'technician', 'validated_by', 'validation_date')


class ZiehlAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = ('sample_id', 'result', 'result_date', 'technician_name', 'validated_by', 'validation_date')


class ReceptionCRLAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = (
        'sample_id', 'batch_id', 'reception_date', 'received_by', 'quantity', 'sample_aspect', 'acceptability',
        'rejection_reason', 'problematic_sample')


class CultureAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = (
        'sample_id', 'positivity_week', 'culture_result', 'gradation', 'result_date', 'technician_name', 'validated_by',
        'validation_date')


class TSAAdmin(admin.ModelAdmin):
    search_fields = ('sample_id',)
    list_display = (
        'sample_id', 'blood_result', 'acceptability', 'result_date', 'technician_code', 'validated_by',
        'validation_date')


class WorksheetAdmin(admin.ModelAdmin):
    search_fields = ('code',)
    list_display = ('code', 'inoculation_date')


class MedicalReviewAdmin(admin.ModelAdmin):
    search_fields = ('participant_id',)
    list_display = ('participant_id', 'tb_case', 'review_date', 'panel_representative')


class CentralCXRReadingAdmin(admin.ModelAdmin):
    list_display = ('participant', 'cxr_reading_result', 'cavity', 'infiltrate', 'lung_nodules', 'pleural_effusion',
                    'mediastinal_lymphadenopathy', 'hilar_lymphadenopathy', 'discipleship', 'cxr_final_reading_result')


class CollisionLogAdmin(admin.ModelAdmin):
    list_display = ('sequence', 'old_household', 'new_household', 'old_pin', 'new_pin')


class CAD4TBSettingsAdmin(admin.ModelAdmin):
    list_display = ('username', 'password', 'domain', 'project', 'archive', 'threshold')


admin.site.register(Census, CensusAdmin)
admin.site.register(Participant, ParticipantAdmin)
admin.site.register(GeneXpert, GeneXpertAdmin)
admin.site.register(Gx_error_log, GxErrorLogAdmin)
admin.site.register(Symptom_Screening, SymptomScreeningAdmin)
admin.site.register(Risk_factor, RiskFactorAdmin)
admin.site.register(XRay, XRayAdmin)
admin.site.register(Sputum_collection, SputumCollectionAdmin)
admin.site.register(HIV_Screening, HIVScreeningAdmin)
admin.site.register(Reception_in, ReceptionInAdmin)
admin.site.register(Reimbursment, ReimbursmentAdmin)
admin.site.register(Cluster, ClusterAdmin)
admin.site.register(Batch_transport, BatchTransportAdmin)
admin.site.register(Batch_sample, BatchSampleAdmin)
admin.site.register(Batch_reception, BatchReceptionAdmin)
admin.site.register(Genotype_CM, GenotypeCMAdmin)
admin.site.register(Genotype_MTBDR, GenotypeMTBDRAdmin)
admin.site.register(Smear, SmearAdmin)
admin.site.register(Ziehl, ZiehlAdmin)
admin.site.register(Reception_CRL, ReceptionCRLAdmin)
admin.site.register(Culture, CultureAdmin)
admin.site.register(TSA, TSAAdmin)
admin.site.register(Worksheet, WorksheetAdmin)
admin.site.register(Medical_review, MedicalReviewAdmin)
admin.site.register(CentralCXRReading, CentralCXRReadingAdmin)
admin.site.register(Collision_log, CollisionLogAdmin)
admin.site.register(CAD4TB_settings, CAD4TBSettingsAdmin)
