import factory
import random
import uuid
import datetime
from .models import Census, Participant
from .choices import *


class CensusFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Census

class ParticipantFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Participant

    name = factory.Faker('first_name')
    surname = factory.Faker('last_name')



def create_census(cluster_code, household_number):
    census = CensusFactory.create(
        uri=uuid.uuid4().hex,
        cluster_code=cluster_code,
        household_number=household_number,
        visit_result_end=random.choice(VISIT_RESULT_CHOICES)[0],
        house_compartments=random.randint(1, 4),
        compartments_for_sleep=random.randint(1, 3),
        visit_date=datetime.date.today())
    return census


def create_particioant(census):
    participant = ParticipantFactory.create(
        uri=uuid.uuid4().hex,
        census=census,
        age=random.randint(15, 50),
        gender=random.choice(GENDER_CHOICES)[0],
        relationship_hh = random.choice(HOUSEHOLD_RELATIONSHIPS_CHOICES)[0],
        occupation = random.randint(1, 10),
        academic_level = random.choice(ACADEMIC_LEVEL_CHOICES)[0],
        marital_status = random.choice(MARITAL_STATUS_CHOICES)[0],
        is_head_of_hh = random.choice(YESNO_CHOICES)[0],
        is_leaving_at_house = random.choice(YESNODONTKNOW_CHOICES)[0],
        is_sleeping_for_weeks = random.choice(YESNODONTKNOW_CHOICES)[0],
        is_elegible_age = random.choice(YESNODONTKNOW_CHOICES)[0])
    return participant

def generate_participants(households, participants, clusters):
    for cluster in clusters:
        for hh in range(1, households+1):
            household_number = str(hh).zfill(3)
            census = create_census  (cluster_code=cluster, household_number=household_number)
            print(census)
            for p in range(participants):
                participant = create_particioant(census=census)
                print("-" + participant.__str__())
