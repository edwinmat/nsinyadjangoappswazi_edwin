# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.db import models
from django.core.validators import MinLengthValidator, RegexValidator, MaxLengthValidator
from .choices import *
from audit_log.models import AuthStampedModel
from django.db.models import Max, ManyToManyField
from django.utils import timezone
from random import randint
import uuid

PARTICIPANT_SEQ_STEP = "1"

PARTICIPANT_SEQ_END_INDEX = 8

PARTICIPANT_SEQ_START_INDEX = 5


class AggregateModel(AuthStampedModel):
    uri = models.CharField(max_length=254)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True


class Census(AggregateModel):
    cluster_code = models.CharField(
        max_length=254,
        validators=[RegexValidator(
            regex=r'^[0-9]*$',
            message="O codigo do cluster deve conter apenas tres digitos numericos (001-999)"
        )])
    household_number = models.CharField(
        max_length=3,
        validators=[RegexValidator(
            regex=r'^[0-9]*$',
            message="O codigo do cluster deve conter apenas tres digitos numericos (001-999)"
        )])

    # The coordinates can be null because is still an open question in the requirements.
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    altitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)

    team_number = models.IntegerField(null=True, blank=True)
    province = models.CharField(max_length=100, null=True, blank=True)
    district = models.CharField(max_length=100, null=True, blank=True)
    administrative_post = models.CharField(max_length=100, null=True, blank=True)
    locality = models.CharField(max_length=100, null=True, blank=True)
    village = models.CharField(max_length=100, null=True, blank=True)

    residence_area = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=RESIDENCE_AREA_CHOICES,
        help_text='Zona onde reside?'
    )

    # household_head_name = models.CharField(max_length=100)
    # household_head_gender = models.CharField(
    #     max_length=254,
    #     choices=GENDER_CHOICES,
    #     help_text='Genero'
    # )

    household_type = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSEHOLD_TYPE_CHOICES,
        help_text='Tipo de Agregado'
    )

    household_type_other = models.CharField(max_length=100, null=True, blank=True)

    visit_result_end = models.CharField(
        max_length=254,
        choices=VISIT_RESULT_CHOICES,
        help_text='Resultado da Visita?',
        blank=True,
        null=True
    )

    visit_result_end_other = models.CharField(max_length=100, null=True, blank=True)

    interviewer_name = models.CharField(max_length=100, null=True, blank=True)

    house_compartments = models.IntegerField(null=True)

    compartments_for_sleep = models.IntegerField(null=True)

    house_building_material = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=BUILDING_MATERIAL_CHOICES,
        help_text='A quem pertence a habitaÁ„o onde vive o agregado familiar?'
    )

    house_building_material_other = models.CharField(max_length=100, null=True, blank=True)

    house_covering_material = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COVERING_MATERIAL_CHOICES,
        help_text=' Material de cobertura?'
    )

    house_covering_material_other = models.CharField(max_length=100, null=True, blank=True)

    house_ownership_type = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSEHOLD_TYPE_CHOICES,
        help_text='Material de construncao?'
    )

    house_owner_ownership_other = models.CharField(max_length=100, null=True, blank=True)

    house_owning_method = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSE_OWNING_METHOD_CHOICES,
        help_text='Se a casa È prÛpria diga como a conseguiu?'
    )

    house_owning_method_other = models.CharField(max_length=100, null=True, blank=True)

    house_pavement_type = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSE_PAVEMENT_CHOICES,
        help_text='Pavimento da casa?'
    )

    house_pavement_type_other = models.CharField(max_length=100, null=True, blank=True)

    lighting_main_source = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=LIGHT_TYPE_CHOICES,
        help_text='Fonte de energia?'
    )

    lighting_main_source_other = models.CharField(max_length=100, null=True, blank=True)

    sanitation_type = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=SANITATION_TYPE_CHOICES,
        help_text='Onde È que os membros do seu agregado familiar fazem  as necessidades maiores??'
    )

    trash_treatment = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=TRASH_TREATMENT_CHOICES,
        help_text='Onde È que os membros do seu agregado familiar fazem  as necessidades maiores??'
    )

    trash_treatment_other = models.CharField(max_length=100, null=True, blank=True)

    water_main_source = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=WATER_MAIN_CHOICES,
        help_text='Onde È que os membros do seu agregado familiar fazem  as necessidades maiores??'
    )
    water_main_source_other = models.CharField(max_length=100, null=True, blank=True)

    visit_result_start = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=VISIT_RESULT_CHOICES,
        help_text='Resultado da Visita?'
    )

    visit_result_start_other = models.CharField(max_length=100, null=True, blank=True)

    visit_date = models.DateField()
    visit_number = models.IntegerField()

    status = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        # unique_together = ('cluster_code', 'household_number')
        verbose_name_plural = 'Census'

    def __str__(self):
        return '%s%s' % (self.cluster_code, self.household_number)


class Participant(AggregateModel):
    census = models.ForeignKey(
        Census,
        verbose_name="Participant's registration reference"
    )

    pin = models.CharField(
        max_length=9,
        unique=False,
        validators=[MinLengthValidator(7, 'O PIN deve conter nove digitos'),
                    MaxLengthValidator(7, 'O PIN deve conter nove digitos')])

    age = models.IntegerField(verbose_name="Idade")

    is_permanent_resident = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=RESIDENCE_PERMANENCE_CHOICES,
        help_text='Residente permanente no agregado?'
    )

    household_surname = models.CharField(max_length=100, null=True, blank=True)

    name = models.CharField(max_length=100)

    surname = models.CharField(max_length=100)

    gender = models.CharField(
        max_length=254,
        choices=GENDER_CHOICES,
        help_text='Genero'
    )

    relationship_hh = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSEHOLD_RELATIONSHIPS_CHOICES,
        help_text='Relacionamento com o agregado?'
    )

    occupation = models.IntegerField(null=True, blank=True)

    academic_level = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=ACADEMIC_LEVEL_CHOICES,
        help_text='Nivel de Academico'
    )

    marital_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=MARITAL_STATUS_CHOICES,
        help_text='Estado Civil?'
    )

    is_head_of_hh = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='E responsavel do agregado?'
    )

    is_leaving_at_house = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Vive habitualmente na casa?'
    )

    is_sleeping_for_weeks = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Dormiu nas ultimas duas semanas?'
    )

    is_elegible_age = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem mais de 15 anos?'
    )

    eligible_for_sputum = models.CharField(null=True, choices=YESNO_CHOICES, blank=True, max_length=2)
    case_control_score = models.IntegerField(null=True, blank=True)
    latest_stage = models.CharField(null=True, blank=True, max_length=20, choices=STAGES)
    creation_date = models.DateTimeField(null=True, blank=True)

    @property
    def review(self):
        return Medical_review.objects.filter(participant_id=self.pin).first()

    @property
    def central_cxr_reading(self):
        return CentralCXRReading.objects.filter(participant=self).first()

    @property
    def is_case_control(self):
        if not self.case_control_score:
            self.case_control_score = randint(0, 100)
            self.save()
        return self.case_control_score < 12

    def is_eligible_for_sputum(self):
        return self.eligible_for_sputum == YES

    def generate_next_pin(self):
        if Participant.objects.count() == 0:
            # return '%s%s001' % (self.census.cluster_code, self.census.household_number)
            next_pin_seq = 1
        else:
            if Participant.objects.filter(census=self.census):
                last_pin_seq = (Participant.objects.filter(census=self.census).aggregate(Max('pin'))['pin__max'])[
                               PARTICIPANT_SEQ_START_INDEX:]
                next_pin_seq = int(last_pin_seq) + 1
            else:
                next_pin_seq = 1

                # if next_pin_seq < 100:
                #     if next_pin_seq < 10:
                #         next_pin_seq = '00' + str(next_pin_seq)
                #     else:
                #         next_pin_seq = '0' + str(next_pin_seq)

        return '%s%s%s' % (self.census.cluster_code,
                           self.census.household_number,
                           str(next_pin_seq).zfill(2))

    def save(self, *args, **kwargs):
        if not self.pin:
            self.pin = self.generate_next_pin()

        super(Participant, self).save(*args, **kwargs)

    def __str__(self):
        return '%s - %s' % (self.pin, self.name)


class Other_household_member(AuthStampedModel):
    id = models.CharField(primary_key=True, max_length=100)

    census = models.ForeignKey(
        Census,
        verbose_name="Participant's registration reference"
    )

    age = models.IntegerField(verbose_name="Idade")

    uri = models.CharField(max_length=100, null=True, blank=True)

    is_permanent_resident = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=RESIDENCE_PERMANENCE_CHOICES,
        help_text='Residente permanente no agregado?'
    )

    household_surname = models.CharField(max_length=100, null=True, blank=True)

    name = models.CharField(max_length=100)

    surname = models.CharField(max_length=100)

    gender = models.CharField(
        max_length=254,
        choices=GENDER_CHOICES,
        help_text='Genero'
    )

    relationship_hh = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HOUSEHOLD_RELATIONSHIPS_CHOICES,
        help_text='Relacionamento com o agregado?'
    )

    academic_level = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=ACADEMIC_LEVEL_CHOICES,
        help_text='Nivel de Academico'
    )

    marital_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=MARITAL_STATUS_CHOICES,
        help_text='Estado Civil?'
    )

    is_head_of_hh = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='E responsavel do agregado?'
    )

    is_leaving_at_house = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Vive habitualmente na casa?'
    )

    is_sleeping_for_weeks = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Dormiu nas ultimas duas semanas?'
    )

    is_elegible_age = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem mais de 15 anos?'
    )

    creation_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s - %s' % (self.pin, self.name)


class HIV_Screening(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    is_test_done_before = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Teste feito antes?'
    )

    last_test_done = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=LAST_TEST_PERIOD_COICES,
        help_text="Quando foi testdo pela ultima vez?"
    )

    test_reveal = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Revelar resultado?'
    )

    what_is_last_result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HIV_RESULT_CHOICES,
        help_text='Resultado do ultimo teste?'
    )

    if_hiv_positive = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Comprovativo?'
    )

    tarv_treatment = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Esta em TARV?'
    )

    tarv_treatment_month = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=MONTH_CHOICES,
        help_text='Mes em TARV?'
    )
    tarv_treatment_year = models.CharField(max_length=4, null=True, blank=True)

    tpi_treatment = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Esta em TPI?'
    )

    tpi_treatment_month = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=MONTH_CHOICES,
        help_text='Mes em TPI?'
    )

    tpi_treatment_year = models.CharField(max_length=4, null=True, blank=True)

    exam_was_consented = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Consetiu o teste?'
    )

    exam_not_consented_reason = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=NOT_CONSENTED_REASON_CHOICES,
        help_text='Razao de nao consetir?'
    )

    specify_other = models.CharField(max_length=100, null=True, blank=True)

    final_result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Quer saber o resultado?'
    )

    determine_result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HIV_RESULT_CHOICES,
        help_text='Resultado Determine?'
    )

    unigold_result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HIV_RESULT_CHOICES,
        help_text='Resultado UNIGOLD?'
    )

    result_automated = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=HIV_RESULT_CHOICES,
        help_text='Resultado?'
    )

    pos_counsellor = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Fez aconselhamento POS?'
    )

    counsellor_name = models.CharField(max_length=100, null=True, blank=True)
    counsellor_category = models.CharField(max_length=100, null=True, blank=True)
    hiv_status_datetime = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.participant.__str__()


class Symptom_Screening(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    survey_date = models.DateField(null=True, blank=True)

    visit_result = models.CharField(
        max_length=254,
        choices=VISIT_RESULT_CHOICES,
        help_text='Resultado da Visita?'
    )

    visit_result_other = models.CharField(max_length=100, null=True, blank=True)

    tb_past = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Ja foi tratado para TB no passado?'
    )

    tb_episode_year = models.CharField(max_length=254, null=True, blank=True)

    tb_treatment_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Esta actuamente em tratamento ?'
    )

    tb_treatment_weeks = models.CharField(max_length=2, null=True, blank=True)

    tb_treatment_local = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=TREATMENT_LOCAL_CHOICES,
        help_text='Onde esta a fazer o tratamento ?'
    )

    tb_treatment_local_other = models.CharField(max_length=100, null=True, blank=True)

    tb_injectable_treatment = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tratamento injectavel ?'
    )

    cough_status = models.CharField(max_length=100, null=True, blank=True)

    tb_injectable_treatment = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem tosse ?'
    )

    cough_weeks = models.CharField(max_length=2, null=True, blank=True)

    cough_sputum = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem tosse com sujidade?'
    )

    sputum_blood = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Sujidade com sangue?'
    )

    chest_pain = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem dor no peito ?'
    )

    chest_pain_weeks = models.CharField(max_length=2, null=True, blank=True)

    fever_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Sente o corpo a aquecer ?'
    )

    fever_weeks = models.CharField(max_length=2, null=True, blank=True)

    night_sweat = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Transpira de noite ?'
    )

    night_sweat_weeks = models.CharField(max_length=2, null=True, blank=True)

    weight_loss = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tem perdido peso?'
    )

    arm_lenght = models.CharField(max_length=3, null=True, blank=True)

    symptom_eligible = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='Elegivel?'
    )

    illness_realise = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Acha que pode ser doenca?'
    )

    health_care_need = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Acha que necessita de ajuda medica?'
    )

    health_care_visit = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='Consultou algum agente/unidade sanitaria'
    )

    health_care_visit_done = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=VISIT_HU_LOCAL_CHOICES,
        help_text='Para onde foi?'
    )

    health_care_visit_done_other = models.CharField(max_length=100, null=True, blank=True)

    health_care_weeks = models.CharField(max_length=2, null=True, blank=True)

    health_care_trip_days = models.CharField(max_length=2, null=True, blank=True)
    health_care_trip_hours = models.CharField(max_length=2, null=True, blank=True)

    health_care_time_days = models.CharField(max_length=2, null=True, blank=True)
    health_care_time_hours = models.CharField(max_length=2, null=True, blank=True)

    sputum_test = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Fez teste de escarro?'
    )

    xray_test = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Fez RX?'
    )

    reason_not_visiting = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=REASON_NOT_VISITING_HU_CHOICES,
        help_text='Razao de nao ter visitado o Centro medico?'
    )

    reason_not_visiting_other = models.CharField(max_length=100, null=True, blank=True)

    participant_read_newspaper = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=RISKFACTOR_MEDIA_CHOICES,
        help_text='Le jornal?'
    )

    def participant_read_newspaper_verbose(self):
        return dict(RISKFACTOR_MEDIA_CHOICES)[self.participant_read_newspaper]

    participant_watch_tv = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=RISKFACTOR_MEDIA_CHOICES,
        help_text='Assites televisao?'
    )

    def participant_watch_tv_verbose(self):
        return dict(RISKFACTOR_MEDIA_CHOICES)[self.participant_watch_tv]

    participant_listen_radio = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=RISKFACTOR_MEDIA_CHOICES,
        help_text='Ouve Radio?'
    )

    def participant_listen_radio_verbose(self):
        return dict(RISKFACTOR_MEDIA_CHOICES)[self.participant_listen_radio]

    participant_occupation = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=OCCUPATION_CHOICES,
        help_text='Qual e a sua ocupacao?'
    )

    def participant_occupation_verbose(self):
        return dict(OCCUPATION_CHOICES)[self.participant_occupation]

    participant_occupation_other = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.participant.__str__()


class GeneXpert(AuthStampedModel):
    uniqueId = models.CharField(max_length=255, )
    systemName = models.CharField(max_length=255, null=True, blank=True)
    hostId = models.CharField(max_length=255, null=True, blank=True)
    exportedDate = models.DateTimeField()
    assay = models.CharField(max_length=255)
    assayVersion = models.CharField(max_length=10)
    sampleId = models.CharField(max_length=45, null=True, blank=True)
    patientId = models.CharField(max_length=255, null=True, blank=True)
    user = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=255)
    testStartedOn = models.DateTimeField()
    testEndedOn = models.DateTimeField()
    reagentLotId = models.CharField(max_length=45, null=True, blank=True)
    cartridgeExpirationDate = models.DateTimeField()
    cartridgeSerial = models.CharField(max_length=45, )
    moduleSerial = models.CharField(max_length=45)
    instrumentSerial = models.CharField(max_length=45)
    softwareVersion = models.CharField(max_length=10, null=True, blank=True)
    resultIdMtb = models.IntegerField()
    resultIdRif = models.IntegerField(null=True, blank=True)
    deviceSerial = models.CharField(max_length=45)
    notes = models.CharField(max_length=255, null=True, blank=True)
    resultText = models.CharField(max_length=255, null=True, blank=True)
    password = models.CharField(max_length=255, null=True, blank=True)
    errorStatus = models.CharField(max_length=255)
    errorCode = models.CharField(max_length=255, null=True, blank=True)
    errorNotes = models.TextField(null=True, blank=True)
    mtb = models.CharField(max_length=2, choices=GENEXPERT_CHOICES)
    rif = models.CharField(max_length=2, choices=RIFAMPICINA_CHOICES, null=True, blank=True)
    positivity = models.CharField(max_length=2, choices=POSITIVITY_CHOICES, null=True, blank=True)
    gx_result = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s-%s' % (self.patientId, self.sampleId)


class Gx_error_log(models.Model):
    error_type = models.CharField(max_length=2, choices=GX_ERROR_TYPES)
    creation_date = models.DateTimeField()
    gx_result = models.TextField()

    def __str__(self):
        return self.get_error_type_display()


class Participant_tracking(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    pin = models.CharField(
        max_length=9,
        unique=True,
        validators=[MinLengthValidator(9, 'O PIN deve conter nove digitos'),
                    MaxLengthValidator(9, 'O PIN deve conter nove digitos')])

    stages = models.CharField(max_length=100)


class XRay(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    xray_tec_code = models.CharField(max_length=100, null=True, blank=True)

    xray_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNOXRAY_CHOICES,
        help_text='Raio-X Feito?'
    )

    xray_status_reason = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=XRAY_NOTDONE_REASON_CHOICES,
        help_text='Motivo da nao realizacao?'
    )

    xray_status_reason_other = models.CharField(max_length=100, null=True, blank=True, )

    xray_results = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=XRAY_RESULT_CHOICES,
        help_text='Resultado do RX?'
    )

    xray_clinician_name = models.CharField(max_length=100, null=True, blank=True)
    xray_date = models.DateTimeField()
    xray_time = models.TimeField()

    CAD4TB_score = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.participant.__str__()


class Reception_in(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    date_field = models.DateTimeField()
    technician_name = models.CharField(max_length=100, null=True, blank=True)

    consent_participation = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='Tem consentimento para o estudo?'
    )

    consent_participation_age = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNO_CHOICES,
        help_text='Tem consentimento para o estudo?'
    )

    phone_number = models.CharField(max_length=100, null=True, blank=True)

    participant_gender = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=GENDER_CHOICES,
        help_text='Genero'
    )

    participant_age = models.CharField(max_length=3, null=True, blank=True)
    participant_dob = models.CharField(max_length=100, null=True, blank=True)

    academic_level = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=ACADEMIC_LEVEL_CHOICES,
        help_text='Nivel Academico'
    )

    def __str__(self):
        return self.participant.__str__()


class Sputum_collection(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    sample_id = models.CharField(max_length=9)
    date_sample = models.DateField()
    techinician_name = models.CharField(max_length=100, null=True, blank=True)

    prod_mode = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=SPUTUM_PROD_MODE_CHOICES,
        help_text='Modo de producao'
    )

    amount_sample = models.CharField(max_length=100, null=True, blank=True)

    aspect_sample = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=SPUTUM_SAMPLE_CHOICES,
        help_text='Aspecto da amostra'
    )

    def __str__(self):
        return '%s' % (self.sample_id)


class Risk_factor(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    participant_workplace_space = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=WORKPLACE_SPACE_CHOICES,
        help_text='Trabalha em ambiente fechado,aberto ou ambos?'
    )

    def participant_workplace_space_verbose(self):
        return dict(WORKPLACE_SPACE_CHOICES)[self.participant_workplace_space]

    work_mine_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Ja alguma vez trabalhou na mina?'
    )

    def work_mine_history_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.work_mine_history]

    work_mine_period = models.CharField(max_length=2, null=True, blank=True)

    work_mine_type = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=MINE_TYPE_CHOICES,
        help_text='Que tipo de mina trabalhou?'
    )

    def work_mine_type_verbose(self):
        return dict(MINE_TYPE_CHOICES)[self.work_mine_type]

    work_prison_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Ja alguma vez trabalhou/ficou na prisao?'
    )

    def work_prison_history_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.work_prison_history]

    work_healthcare_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Ja alguma vez trabalhou no centro de saude?'
    )

    def work_healthcare_history_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.work_healthcare_history]

    province_visit = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Viajou para alguma provincia(ultimo 12 meses)?'
    )

    def province_visit_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.province_visit]

    outside_moz_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Ja esteve fora de Mocambique?'
    )

    def outside_moz_history_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.outside_moz_history]

    other_country = models.CharField(max_length=100, null=True, blank=True)
    smoking_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Fuma tabaco?'
    )

    def smoking_status_verbose(self):
        return dict(YESNODONTKNOW_CHOICES)[self.smoking_status]

    cigarette_amount = models.CharField(max_length=2, null=True, blank=True)

    smoking_period = models.CharField(max_length=2, null=True, blank=True)

    smoking_frequency_inhouse = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=SMOKING_FREQUENCY_CHOICES,
        help_text='Com que frequencia fuma?'
    )

    def smoking_frequency_inhous_verbose(self):
        return dict(SMOKING_FREQUENCY_CHOICES)[self.smoking_frequency_inhouse]

    drinking_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='JA alguma vez consumiu bebida alcoolica?'
    )

    drinking_frequency = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_FREQUENCY_CHOICES,
        help_text='Com que frequencia consome bebidas ?'
    )

    drinking_frequency_daily = drinking_amount_occasion = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_AMOUNT_CHOICES,
        help_text='Quantas bebidas consome num dia normal?'
    )

    drinking_amount_occasion = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_FREQUENCY_CHOICES,
        help_text='Com que frequencia consome 6 bebidas ou mais ?'
    )

    drinking_task_failure_frequency = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_FREQUENCY_CHOICES,
        help_text=' Com que frequencia nao conseguiu executar tarefas por causa da bebida?'
    )
    drinking_memory_failure_frequency = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_FREQUENCY_CHOICES,
        help_text='Com que frequencia nao se lembrou do que aconteceu na noite anterior?'
    )

    drinking_concern = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=DRINKING_FREQUENCY_CHOICES,
        help_text='Ja alguma vez um familiar, amigo, medico ou outro profissional de saude manifestou preocupacao pelo seu consumo de alcool ou sugeriu que deixasse de beber?'
    )

    household_sickness_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Tinha alguem doente em casa nos ultimos cinco anos?'
    )

    tb_care_history = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Cuidou alguem com tuberculose na sua casa nos ultimos dois anos?'
    )

    people_tb_patient_meals = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text=' Pessoas comem ou bebem com pessoas com TB?'
    )

    people_tb_patient_comfort_status = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas se sentem confortaveis em estar priximo de pessoas com TB?'
    )

    community_tb_patient_behaviour = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Quando pessoas têm TB, membros da comunidade se comportam de forma diferente em relação a essa pessoa?'
    )

    people_children_tb_patient = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text=' Pessoas aceitam que pessoas com TB brinquem com seus filhos?'
    )

    people_tb_patient_distance = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas se mantém distantes de pessoas com TB.?'
    )

    people_tb_patient_unpleasant = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas pensam que os tuberculosos são desagradáveis?'
    )

    people_tb_patient_conversation = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas falam com pessoas com TB?'
    )

    people_tb_patient_fear = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas têm medo de pessoas com TB?'
    )

    peoplte_tb_patient_avoid = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas evitam tocar em pessoas com TB?'
    )

    tb_patient_disease_responsability = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas aceitam ter pessoas com TB a viver nas suas comunidades.?'
    )

    people_tb_patient_acceptance = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=COMMUNITY_TB_CHOICES,
        help_text='Pessoas aceitam ter pessoas com TB a viver nas suas comunidades?'
    )

    creation_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.participant.__str__()


class Reimbursment(AggregateModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant's ID"
    )

    date_end_field = models.DateField()

    technician_name = models.CharField(max_length=100, null=True, blank=True)

    us_reference = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Referida pa a US?'
    )

    transport_reimbursement = models.CharField(
        max_length=254,
        choices=YESNODONTKNOW_CHOICES,
        help_text='Reembolso do valor de transporte?'
    )
    amount = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.participant.__str__()


class Cluster(models.Model):
    code = models.CharField(max_length=2, unique=True)
    status = models.CharField(
        max_length=16,
        choices=CLUSTER_STATUS_CHOICES,
        help_text='Estado do cluster',
        default=2,
    )
    open_date = models.DateTimeField(null=True, blank=True)
    close_date = models.DateTimeField(null=True, blank=True)

    def open(self):
        for i in range(1, 100):
            batch_id = self.code + format(i, '0>4')
            batch = Cluster_batch(cluster_id=self.id, batch_id=batch_id)
            batch.save()
        self.status = OPEN
        self.open_date = timezone.now()
        self.save()

    def close(self):
        self.status = CLOSED
        self.close_date = timezone.now()
        self.save()

    def __str__(self):
        return '%s' % (self.code)


class Cluster_batch(models.Model):
    cluster = models.ForeignKey(
        Cluster,
        verbose_name="Cluster's ID"
    )
    batch_id = models.CharField(max_length=6)
    used = models.BooleanField(default=False)
    date_used = models.DateTimeField(null=True)

    def use_it(self):
        self.used = True
        self.date_used = timezone.now()
        self.save()

    def __str__(self):
        return '%s - %s' % (self.cluster.code, self.batch_id)


class Batch_transport(AggregateModel):
    batch_id = models.CharField(max_length=9)
    packaged_by = models.CharField(max_length=100, null=True, blank=True)
    packaging_date = models.DateField()
    total_sent_samples = models.CharField(max_length=100, null=True, blank=True)
    send_date = models.DateTimeField(null=True)
    temperature = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    handed_over_by = models.CharField(max_length=100, null=True)
    handover_date = models.DateTimeField(null=True, )
    carrier_driver_name = models.CharField(max_length=100, null=True)
    carrier_reception_date = models.DateTimeField(null=True)

    @property
    def reception(self):
        return Batch_reception.objects.filter(batch_id=self.batch_id).first()

    @property
    def samples(self):
        return Reception_CRL.objects.filter(batch_id=self.batch_id)

    def __str__(self):
        return '%s' % (self.batch_id)


class Batch_sample(models.Model):
    batch_id = models.CharField(max_length=9)
    sample_id = models.CharField(max_length=9)

    def __str__(self):
        return '%s - %s' % (self.batch_id, self.sample_id)


class Batch_reception(AuthStampedModel):
    batch_id = models.CharField(max_length=9)
    total_received_samples = models.IntegerField()
    temperature = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    received_by = models.CharField(max_length=100)
    reception_date = models.DateTimeField()

    def __str__(self):
        return self.batch_id


class Reception_CRL(AuthStampedModel):
    sample_id = models.CharField(max_length=9)
    batch_id = models.CharField(max_length=9, null=True, blank=True)
    reception_date = models.DateField()
    received_by = models.CharField(max_length=100)
    quantity = models.DecimalField(max_digits=3, decimal_places=2, null=True, blank=True)
    comments = models.CharField(max_length=200, null=True, blank=True)
    sample_aspect = models.CharField(
        max_length=32,
        choices=SPUTUM_SAMPLE_CHOICES,
        help_text='Aspecto da amostra?'
    )
    acceptability = models.CharField(
        max_length=16,
        choices=YESNO_CHOICES,
        help_text='Aceitabilidade?'
    )
    rejection_reason = models.CharField(
        max_length=32,
        choices=REJECTION_CRL_CHOICES,
        help_text='Aceitabilidade?',
        null=True
    )
    problematic_sample = models.CharField(
        max_length=100,
        choices=PROBLEMATIC_SAMPLE_CHOICES,
        help_text='Aceitabilidade?',
        null=True
    )

    @property
    def sputum(self):
        return Sputum_collection.objects.filter(sample_id=self.sample_id).first()

    def __str__(self):
        return self.sample_id


class Genotype_CM(AggregateModel):
    sample_id = models.CharField(max_length=9)
    result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=GENOTYPE_CM_CHOICES,
        help_text='Resultado?'
    )
    technician_name = models.CharField(max_length=100, null=True, blank=True)
    tube = models.CharField(max_length=1, choices=CULTURE_TUBES)
    result_date = models.DateField()
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class Genotype_MTBDR(AggregateModel):
    sample_id = models.CharField(max_length=9)
    result = models.CharField(
        max_length=254,
        null=True,
        blank=True,
        choices=GENOTYPE_MT_CHOICES,
        help_text='Resultado?'
    )
    technician_name = models.CharField(max_length=100, null=True, blank=True)
    tube = models.CharField(max_length=1, choices=CULTURE_TUBES)
    result_date = models.DateField()
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class Smear(AggregateModel):
    sample_id = models.CharField(max_length=9)
    bacil = models.CharField(
        max_length=32,
        choices=BACILOSCOPY_CHOICES,
        help_text='Resultados da Baciloscopia?'
    )
    technician = models.CharField(max_length=100)
    result_date = models.DateField()
    result_time = models.TimeField(null=True, blank=True)
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class Ziehl(AggregateModel):
    sample_id = models.CharField(max_length=9)
    result = models.CharField(
        max_length=32,
        choices=ZIEHL_CHOICES,
        help_text='Resultado?'
    )
    technician_name = models.CharField(max_length=100)
    tube = models.CharField(max_length=1, choices=CULTURE_TUBES)
    result_date = models.DateField()
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class Worksheet(AuthStampedModel):
    code = models.CharField(max_length=40)
    inoculation_date = models.DateField(null=True, blank=True)
    sequence = models.IntegerField()
    type = models.CharField(max_length=32,
                            choices=WORKSHEET_TYPE,
                            help_text='Tipo de worksheet?'
                            )

    def __str__(self):
        return self.code

    @property
    def stringId(self):
        return str(self.id)


class Worksheet_Sample(AuthStampedModel):
    sample_id = models.CharField(max_length=9)
    worksheet_code = models.CharField(max_length=40)

    @property
    def stringId(self):
        return str(self.id)


class Culture(AuthStampedModel):
    sample_id = models.CharField(max_length=9)
    tube = models.CharField(max_length=1, choices=CULTURE_TUBES)
    positivity_week = models.CharField(
        max_length=32,
        choices=POSITIVITY_WEEK_CHOICES,
        help_text='Tempo de positividade?'
    )
    culture_result = models.CharField(
        max_length=32,
        choices=CULTURE_RESULT_CHOICES,
        help_text='Resultados da Cultura?'
    )
    gradation = models.CharField(
        max_length=32,
        choices=GRADATION_CHOICES,
        null=True,
        blank=True,
        help_text='Gradação?'
    )
    technician_name = models.CharField(max_length=100)
    result_date = models.DateField()
    update_date = models.DateTimeField(null=True, blank=True)
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class TSA(AuthStampedModel):
    sample_id = models.CharField(max_length=9)
    blood_result = models.CharField(
        max_length=32,
        choices=BLOOD_RESULT_CHOICES,
        help_text='Resultado da placa Agar Sangue?'
    )
    acceptability = models.CharField(
        max_length=16,
        choices=YESNO_CHOICES,
        help_text='Aceitabilidade?'
    )
    TSA_1_H_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_1_R_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_1_Z_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_1_E_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_1_S_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_2_Am_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_2_Ka_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_2_Cm_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    TSA_2_Ofl_result = models.CharField(
        max_length=32,
        choices=TSA_RESULT_CHOICES,
        null=True,
        blank=True
    )
    technician_code = models.CharField(max_length=100)
    result_date = models.DateField()
    validated_by_name = models.CharField(max_length=100, null=True, blank=True)
    validated_by = models.ForeignKey(get_user_model(), null=True, blank=True)
    validation_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.sample_id


class Medical_review(AuthStampedModel):
    participant_id = models.CharField(max_length=9)
    tb_case = models.CharField(max_length=2, choices=YESNO_CHOICES, help_text='It is a TB case?')
    review_date = models.DateField()
    panel_representative = models.CharField(max_length=100)
    remarks = models.TextField(max_length=500)

    def __str__(self):
        return self.participant_id


class CentralCXRReading(AuthStampedModel):
    participant = models.ForeignKey(
        Participant,
        verbose_name="Participant"
    )

    cxr_reading_result = models.CharField(
        choices=XRAY_RESULT_CHOICES,
        max_length=1,
        verbose_name='Resultado do Raio-X'
    )

    cavity = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Cavidade'
    )

    infiltrate = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Infiltrado'
    )

    lung_nodules = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Nódulos Pulmonares'
    )

    pleural_effusion = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Derrame Pleural'
    )

    mediastinal_lymphadenopathy = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Linfadenopatia Mediastinal'
    )

    hilar_lymphadenopathy = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Linfadenopatia Hilar'
    )

    discipleship = models.CharField(
        choices=CXR_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Sequelas'
    )

    cxr_final_reading_result = models.CharField(
        choices=CXR_FINAL_READING_SIDES_CHOICES,
        max_length=1,
        blank=True,
        null=True,
        verbose_name='Resultado Final'
    )

    reading_status = models.CharField(
        choices=CXR_READING_STATUS_CHOICES,
        max_length=1,
        verbose_name='Estado da Leitura'
    )

    creation_date = models.DateTimeField(null=True, blank=True)
    update_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'nsinya_central_cxr_reading'

    def __str__(self):
        return self.participant.pin


class Collision_log(AuthStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sequence = models.IntegerField()
    old_household = models.CharField(max_length=5)
    new_household = models.CharField(max_length=5)
    old_pin = models.CharField(max_length=7)
    new_pin = models.CharField(max_length=7)

    class Meta:
        get_latest_by = 'sequence'

    @property
    def participant(self):
        try:
            return Participant.objects.get(pin=self.new_pin)
        except Participant.DoesNotExist:
            return None


class CAD4TB_settings(models.Model):
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    domain = models.CharField(max_length=100)
    project = models.CharField(max_length=100)
    archive = models.CharField(max_length=100)
    study_url = models.CharField(max_length=100)
    series_url = models.CharField(max_length=100)
    score_url = models.CharField(max_length=100)
    score_type = models.CharField(max_length=100)
    reading_url = models.CharField(max_length=100)
    threshold = models.IntegerField()
