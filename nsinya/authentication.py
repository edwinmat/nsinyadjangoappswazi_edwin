from django.contrib.auth import authenticate
from rest_framework import authentication
from rest_framework import exceptions
from requests.auth import AuthBase

class CAD4TBAuthentication(AuthBase):

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __call__(self, r):
        r.headers['X-USERNAME'] = self.username
        r.headers['X-PASSWORD'] = self.password
        return r


class GxAlertAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):

        username = request.META['HTTP_USERNAME']
        password = request.META['HTTP_PASSWORD']

        if not username:
            return None

        user = authenticate(username=username, password=password)

        if user is None:
            raise exceptions.AuthenticationFailed(_('Invalid username/password.'))

        if not user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (user, None)