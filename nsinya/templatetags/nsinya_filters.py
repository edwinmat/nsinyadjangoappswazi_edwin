from django import template

from nsinya.models import Participant, Reimbursment, GeneXpert, Sputum_collection, HIV_Screening, XRay, Risk_factor, \
    Symptom_Screening, Reception_in, Reception_CRL, Worksheet, Culture, POSITIVITY_WEEK_CHOICES, Ziehl, Genotype_CM, \
    Smear, Genotype_MTBDR

register = template.Library()


@register.filter(name='get_date_sample')
def get_date_sample(value):

    sputum_qs = Sputum_collection.objects.filter(sample_id=value)

    if sputum_qs:
        return sputum_qs.first().date_sample

    return 'N/A'

@register.filter(name='get_date_aspect')
def get_date_aspect(value):

    sputum_qs = Sputum_collection.objects.filter(sample_id=value)

    if sputum_qs:
        return sputum_qs.first().get_aspect_sample_display()
    else:
        reception_qs = Reception_CRL.objects.filter(sample_id=value)

        if reception_qs:
            return reception_qs.first().get_sample_aspect_display()

    return 'N/A'

@register.filter(name='get_date_volume')
def get_date_aspect(value):

    reception_qs = Reception_CRL.objects.filter(sample_id=value)

    if reception_qs:
        return reception_qs.first().quantity

    return 'N/A'

@register.filter(name='get_inoculation_date')
def get_inoculation_date(value):

    worksheet_qs = Worksheet.objects.filter(code=value)

    if worksheet_qs:
        return worksheet_qs.first().inoculation_date
    else:
        if value != '':
            worksheet_qs = Worksheet.objects.filter(id=value)

            if worksheet_qs:
                return worksheet_qs.first().inoculation_date

    return 'N/A'

@register.filter(name='get_worksheet_type')
def get_worksheet_type(value):

    worksheet_qs = Worksheet.objects.filter(code=value)

    if worksheet_qs:
        return worksheet_qs.first().get_type_display()

    return 'N/A'


@register.filter(name='get_date_reception_date')
def get_date_reception_date(value):

    reception_qs = Reception_CRL.objects.filter(sample_id=value)

    if reception_qs:
        return reception_qs.first().reception_date

    return  'N/A'


@register.filter(name='get_date_positivity_week')
def get_date_positivity_week(value):

    culture_qs = Culture.objects.filter(sample_id=value)

    if culture_qs:
        return culture_qs.first().get_positivity_week_display()

    return 'N/A'

@register.filter(name='get_date_gradation')
def get_date_gradation(value):

    culture_qs = Culture.objects.filter(sample_id=value)

    if culture_qs:
        return culture_qs.first().get_gradation_display()

    return 'None'

@register.filter(name='get_date_ziehl_result')
def get_date_ziehl_result(value):

    ziehl_qs = Ziehl.objects.filter(sample_id=value)

    if ziehl_qs:
        return ziehl_qs.first().get_result_display()

    return 'None'


@register.filter(name='get_date_genoCM_result')
def get_date_genoCM_result(value):

    genocm_qs = Genotype_CM.objects.filter(sample_id=value)

    if genocm_qs:
        return genocm_qs.first().get_result_display()

    return 'None'

@register.filter(name='get_date_smear_result')
def get_date_smear_result(value):

    smear_qs = Smear.objects.filter(sample_id=value)

    if smear_qs:
        return smear_qs.first().get_bacil_display()

    return 'None'

@register.filter(name='get_date_genoMT_result')
def get_date_genoCM_result(value):

    genomt_qs = Genotype_MTBDR.objects.filter(sample_id=value)

    if genomt_qs:
        return genomt_qs.first().get_result_display()

    return 'None'

