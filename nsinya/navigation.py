from django.core.signals import request_started
from django.dispatch import receiver

from xf_system.views import XFNavigationViewMixin
from xf_system.xf_navigation import add_navigation
from .security import *


def load_navigation(sender, navigation_trees, request):

    # Each app can create one navigation tree. Navigation trees from multiple apps cannot be merged, by design
    # In this example, the app name is "library"
    #if not navigation_trees.has_key("library"):
    # PYTHON3 UPDATE
    if request.user.is_authenticated:
        if not "nsinya" in navigation_trees:
            navigation_tree = []
            navigation_trees["nsinya"] = navigation_tree

        #PermissionMixin.user_has_model_perm(Book, "view")

        navigation_tree = navigation_trees["nsinya"]
        # Census
        add_navigation(navigation_tree, 'Nsinya', "Census", "/nsinya/census/participants", "fa-group", "Participants")
        add_navigation(navigation_tree, 'Nsinya', "Census", "/nsinya/census/households/", "fa-group", "Households")
        add_navigation(navigation_tree, 'Nsinya', "Census", "/nsinya/census/eligibility/", "fa-group", "Eligibility")

        if is_in_groups(request, CRL_LAB_GROUPS):
            # Central Lab
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/reception", "fa-flask", "Reception")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "", "fa-flask", "Worksheet")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/worksheet", "fa-flask", "Create",
                           "Worksheet")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/worksheetlist", "fa-flask", "List",
                           "Worksheet")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "", "fa-flask", "Culture")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/inoculation", "fa-flask", "Inoculation",
                           "Culture")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/culture_reading", "fa-flask", "Reading",
                           "Culture")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/culture_validation", "fa-flask", "Validation",
                           "Culture")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/smear", "fa-flask", "Smear")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/tsa", "fa-flask", "TSA")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "", "fa-flask", "GenoType")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/genotype_cm", "fa-flask", "CM",
                           "GenoType")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/genotype_mt", "fa-flask", "MTDBR",
                           "GenoType")
            add_navigation(navigation_tree, 'Nsinya', "Central Lab", "/nsinya/crl/ziehl", "fa-flask", "Ziehl Neelsen")

        # Diagnosis
        add_navigation(navigation_tree, 'Nsinya', "Diagnosis", "/nsinya/radiology/central_cxr_reading_list", "fa-user-md", "Central CXR Reading")
        add_navigation(navigation_tree, 'Nsinya', "Diagnosis", "/nsinya/diagnosis/medical_review_panel", "fa-user-md", "Medical Panel Review")

        # Configuration
        if is_in_groups(request, CONFIG_GROUPS):
            add_navigation(navigation_tree, 'Nsinya', "Configuration", "/nsinya/config/clusters", "fa-cogs", "Clusters")
        return

XFNavigationViewMixin.navigation_tree_observers.append(load_navigation)