import requests
import ast
from nsinya.authentication import CAD4TBAuthentication
from nsinya.choices import *
from nsinya.models import XRay, CAD4TB_settings


def authenticate(username, password):
    return CAD4TBAuthentication(username=username, password=password)


def get_content(url, username, password):
    return requests.get(url, auth=authenticate(username=username, password=password))


def get_url(settings: CAD4TB_settings, type, pin, study=None, series=None):
    url = settings.domain
    project = '?Project=%s&Archive=%s&PatientID=%s' % (settings.project, settings.archive, pin)
    url_mapping = {
        "study": '%s%s' % (settings.study_url, project),
        "series": '%s%s&StudyInstanceUID=%s' % (settings.series_url, project, study),
        "score": '%s%s&StudyInstanceUID=%s&SeriesInstanceUID=%s&Type=%s' % (
            settings.score_url, project, study, series, settings.score_type),
        "reading": '%s%s&StudyInstanceUID=%s&SeriesInstanceUID=%s' % (settings.reading_url, project, study, series)
    }
    return '%s%s' % (url, url_mapping[type])


def get_study(settings: CAD4TB_settings, pin):
    study_instance_UID = None
    url = get_url(settings=settings, type='study', pin=pin)
    response = get_content(url=url, username=settings.username, password=settings.password)
    if response.status_code == 200:
        pos = len(response.json()) - 1
        study_instance_UID = response.json()[pos]['StudyInstanceUID']
    return study_instance_UID


def get_series(settings: CAD4TB_settings, pin, study):
    series_instance_UID = None
    url = get_url(settings=settings, type='series', pin=pin, study=study)
    response = get_content(url=url, username=settings.username, password=settings.password)
    if response.status_code == 200:
        if len(response.json()):
            series_instance_UID = response.json()[0]['SeriesInstanceUID']
    return series_instance_UID


def get_cad4tb_score(settings: CAD4TB_settings, pin, study, series):
    score = None
    url = get_url(settings=settings, type='score', pin=pin, study=study, series=series)
    response = get_content(url=url, username=settings.username, password=settings.password)
    if response.status_code == 200:
        if len(response.json()):
            score = response.json()[0]['value']
    return score


def get_cxr_reading(settings: CAD4TB_settings, pin, study, series):
    reading = None
    url = get_url(settings=settings, type='reading', pin=pin, study=study, series=series)
    response = get_content(url=url, username=settings.username, password=settings.password)
    if response.status_code == 200:
        if len(response.json()):
            entry = ast.literal_eval(response.json()[0]['entry'])
            reading = entry.get('category')[0]
    return reading


def get_result_by_cxr_reading(settings: CAD4TB_settings, pin):
    response = get_content(url=get_url(settings=settings, type='study', pin=pin), username=settings.username,
                           password=settings.password)
    if response.status_code == 200:
        studies = response.json()
        for study in studies:
            try:
                study_id = study['StudyInstanceUID']
                series = get_series(settings=settings, pin=pin, study=study_id)
                if series is not None:
                    reading = get_cxr_reading(settings=settings, pin=pin, study=study_id, series=series)
                    if reading is not None and reading != XRAY_RESULTS_4:
                        score = get_cad4tb_score(settings=settings, pin=pin, study=study_id, series=series)
                        break
            except TypeError:
                reading = None
                score = None

    return {'reading': reading, 'score': score}


def update_xray_by_cxr_reading(xray: XRay, pin):
    settings = CAD4TB_settings.objects.all().last()
    result = get_result_by_cxr_reading(settings=settings, pin=pin)
    xray.CAD4TB_score = result['score']
    xray.xray_results = result['reading']
    xray.save()


def update_xray(xray: XRay, pin):
    settings = CAD4TB_settings.objects.all().first()
    study = get_study(settings=settings, pin=pin)
    if study:
        series = get_series(settings=settings, pin=pin, study=study)
        print(series)
        if series:
            score = get_cad4tb_score(settings=settings, pin=pin, study=study, series=series)
            reading = get_cxr_reading(settings=settings, pin=pin, study=study, series=series)

            if reading == 'Normal':
                reading = XRAY_RESULTS_1
            elif reading == 'Abnormal (Sugestivo de TB pulmonar)':
                reading = XRAY_RESULTS_2
            elif reading == 'Outra anormalidade':
                reading = XRAY_RESULTS_3
            elif reading == 'Não interpretável (Repetir Rx, maximo 3X)':
                reading = XRAY_RESULTS_4

            xray.CAD4TB_score = score
            xray.xray_results = reading
            xray.save()
