from rest_framework import exceptions
from nsinya.models import Participant, Sputum_collection, GeneXpert, Gx_error_log
from rest_framework.renderers import JSONRenderer
from django.utils import timezone
from nsinya.choices import *

def get_mtb(id):
    mtb_mapping = {2: '4', 3: '3', 4: '2', 5: '1', 6: '5', 7: '6', 8: '7'}
    try:
        mtb = mtb_mapping[id]
    except KeyError:
        raise exceptions.ValidationError('Invalid ID for MTB result: ' + str(id))
    return mtb


def get_rif(id):
    if id:
        rif_mapping = {2: '4', 3: '5', 4: '1', 5: '2', 6: '3', 7: '6', 8: '7'}
        try:
            rif = rif_mapping[id]
        except KeyError:
            raise exceptions.ValidationError('Invalid ID for FIR result: ' + str(id))
        return rif


def get_positivity(result):
    pos_mapping = {'HIGH': '1', 'MEDIUM': '2', 'LOW': '3', 'VERY LOW': '4', 'ALTO': '1', 'MÉDIO': '2', 'BAIXO': '3',
                   'MUITO BAIXO': '4'}

    if "DETECT" not in result:
        return None

    try:
        data = result.split("|")
        mtb = data[0].split()
        if "NOT" in mtb or "NÃO" in mtb:
            return None

        index = 1
        if "MTB" in mtb:
            index = 2
        key = mtb[index]
        if key == "VERY" or key == "MUITO":
            key = key + " " + mtb[index + 1]
        pos = pos_mapping[key]
    except KeyError:
        raise exceptions.ValidationError('Invalid value for positivity: ' + key)
    return pos


def create_gx(serializer):
    error = None
    sample_id = serializer.validated_data['sampleId']

    if GeneXpert.objects.filter(sampleId=sample_id).exists():
        error = REPEATED_RESULT
    elif not Sputum_collection.objects.filter(sample_id=sample_id).exists():
        error = INVALID_SAMPLE_ID

    data = JSONRenderer().render(serializer.validated_data)
    if error is None:
        mtb = get_mtb(serializer.validated_data['resultIdMtb'])
        rif = get_rif(serializer.validated_data['resultIdRif'])
        positivity = get_positivity(serializer.validated_data['resultText'])
        serializer.save(patientId=sample_id[:7], mtb=mtb, rif=rif, positivity=positivity, gx_result=data)
    else:
        Gx_error_log(error_type=error, creation_date=timezone.now(), gx_result=data).save()
