# -*- coding: utf-8 -*-
from nsinya.models import *
from rest_framework import status
from rest_framework.response import Response
from . import cad4tb_utils
from nsinya.choices import *

OK = '200'
PARTICIPANT_DOES_NOT_EXIST = 'Participante com PIN %s não existe'
PARTICIPANT_IS_MISSING_STATION = 'Participante com PIN %s não passou pela estação de %s'
STATION_ALREADY_DONE = "O Participante ja passou por esta estação"
RISK_FACTOR_PARTICIPANT_NOT_ALLOWED_TO_DO_THIS_STATION = "O parcitipante com PIN %s não deve fazer esta estação, encaminhe ele para o reembolso"
S2_IS_MISSING = "O particioante %s ainda não colectou a segunda amostra"


def validate_participant(pin):
    try:
        participant = Participant.objects.get(pin=pin)
    except Participant.DoesNotExist:
        return None
    return participant


class StageStatusValidation:

    participant = None
    pin = None

    def validate_stage(self, form_id, pin):

        form_ids = ("F01", "F02", "F03", "F04", "F05", "F051", "F06", "F08", "F10", "LAB", "CHECK_HOUSEHOLD")

        stage_validation_mapping = {
            "F02": self.check_eligibility_for_reception_in,
            "F03": self.check_eligibility_for_symptom_screening,
            "F04": self.check_eligibility_for_risk_factors,
            "F05": self.check_eligibility_for_xray_screening,
            "F051": self.check_eligibility_for_xray_reading,
            "F06": self.check_eligibility_for_sputum_collection,
            "F08": self.check_eligibility_for_htc,
            "F10": self.check_eligibility_for_reimbursment,
            "LAB": self.validate_sample,
            "CHECK_HOUSEHOLD": self.check_household_member
        }

        form_id = form_id.upper()
        self.pin = pin

        if form_id not in form_ids:
            return Response(data='Invalid form id', status=status.HTTP_400_BAD_REQUEST)

        if form_id != 'LAB':
            self.participant = validate_participant(self.pin)
            if not self.participant:
                return Response(data='Participant does not exist', status=status.HTTP_404_NOT_FOUND)

        return stage_validation_mapping[form_id]()

    def validate_sample(self):
        try:
            Reception_CRL.objects.get(sample_id=self.pin)
        except Reception_CRL.DoesNotExist:
            return Response(data='Não existe nenhuma amostra com o código %s' % self.pin, status=status.HTTP_404_NOT_FOUND)
        return Response(data="OK")

    def check_eligibility_for_reception_in(self):
        reception_in = Reception_in.objects.filter(participant=self.participant)
        cluster_valid = Cluster.objects.filter(code=self.pin[0:2])

        if cluster_valid and cluster_valid.first().status == "1":
            if reception_in:
                return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
            return Response(data=self.get_participant_details_for_odk(), status=status.HTTP_200_OK)
        else:
            return Response(data="O código de conglomerado ainda não se encontra activo, por favor contacte o IT",
                        status=status.HTTP_404_NOT_FOUND)

    def check_household_member(self):
        relatives = Participant.objects.filter(pin__contains=self.pin[0:5], reception_in__isnull=True)\
            .exclude(pin__exact=self.pin)
        if relatives:
            for relative in relatives:
                relatives_names = " " + relative.__str__() + ", "

            return Response(data="Os membros " + relatives_names + "ainda não passaram pela recepção",
                        status=status.HTTP_404_NOT_FOUND)
        return Response(data="Continuar",status=status.HTTP_404_NOT_FOUND)

    def check_eligibility_for_reimbursment(self):
        reimbursement_qs = Reimbursment.objects.filter(participant=self.participant)
        if reimbursement_qs:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)

        if self.participant.is_eligible_for_sputum():
            sputm_qs = Sputum_collection.objects.filter(participant=self.participant)

            s2_exist = False
            for sputum in sputm_qs:
                if 'S2' in sputum.sample_id:
                    s2_exist = True

            if not s2_exist:
                return Response(data=S2_IS_MISSING % self.participant.pin, status=status.HTTP_404_NOT_FOUND)

        return self.validate_reception_in()


    def check_eligibility_for_htc(self):
        hiv = HIV_Screening.objects.filter(participant=self.participant)
        if hiv:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
        try:
            # TODO: Talk with Angelo to refactor this
            XRay.objects.get(participant=self.participant)
        except XRay.DoesNotExist:
            return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Raio-X"),
                            status=status.HTTP_404_NOT_FOUND)
        return Response(data=self.get_participant_details_for_odk(), status=status.HTTP_200_OK)

    def check_eligibility_for_sputum_collection(self):
        try:
            xray = XRay.objects.get(participant=self.participant)
            ss = Symptom_Screening.objects.get(participant=self.participant)
            if ss.symptom_eligible == NO:
                # TODO: Refactor this code: duplicated code
                if xray.xray_status == '2' or xray.xray_status == '3':
                    self.participant.eligible_for_sputum = YES
                    self.participant.save()
                    return Response(data=self.get_participant_details_for_odk(), status=status.HTTP_200_OK)
                if not xray.CAD4TB_score:
                    return Response(data="O resultado de CAD4TB ainda não está disponivel - contacte o suporte "
                                         "informático",
                                    status=status.HTTP_404_NOT_FOUND)
                if xray.CAD4TB_score < 40 and (xray.xray_results == XRAY_RESULTS_1 or xray.xray_results == XRAY_RESULTS_3):
                    self.participant.eligible_for_sputum = NO
                    self.participant.save()
                    return Response(data="O Participante não é elegivel para colheita de amostra",
                                    status=status.HTTP_200_OK)
        except XRay.DoesNotExist:
            return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Raio-X"),
                            status=status.HTTP_404_NOT_FOUND)

        self.participant.eligible_for_sputum = YES
        self.participant.save()
        return Response(data=self.get_participant_details_for_odk(), status=status.HTTP_200_OK)

    def check_eligibility_for_symptom_screening(self):
        symptom_screening = Symptom_Screening.objects.filter(participant=self.participant)
        if symptom_screening:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
        return self.validate_reception_in()

    def check_eligibility_for_xray_screening(self):
        xray = XRay.objects.filter(participant=self.participant)
        if xray:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
        try:
            if self.participant.is_case_control:
                Risk_factor.objects.get(participant=self.participant)
            else:
                Symptom_Screening.objects.get(participant=self.participant)
        except Risk_factor.DoesNotExist:
            return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Factores de risco"),
                            status=status.HTTP_404_NOT_FOUND)
        except Symptom_Screening.DoesNotExist:
            return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Rastreio de Sintomas"),
                            status=status.HTTP_404_NOT_FOUND)
        return Response(data=self.get_participant_details_for_odk(), status=status.HTTP_200_OK)

    def check_eligibility_for_xray_reading(self):
        cad4tb_utils.get_cad4tb_score(pin=self.pin)
        xray_qs = XRay.objects.filter(participant=self.participant)
        if xray_qs and xray_qs.first().xray_status == YES and xray_qs.first().xray_results != None:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
        if xray_qs:
            xray = xray_qs.first()
        else:
            return Response(data=status.HTTP_404_NOT_FOUND, status=status.HTTP_404_NOT_FOUND)

        if xray.xray_status == "1":
            data = self.get_participant_details_for_odk()
        else:
            data = xray.get_xray_status_display()
        return Response(data=data, status=status.HTTP_200_OK)


    def check_eligibility_for_risk_factors(self):
        risk_factor = Risk_factor.objects.filter(participant=self.participant)
        if risk_factor:
            return Response(data=STATION_ALREADY_DONE, status=status.HTTP_404_NOT_FOUND)
        try:
            Symptom_Screening.objects.get(participant=self.participant)
            if self.participant.is_case_control:
                response = Response(data=self.get_participant_details_for_odk(),
                                    status=status.HTTP_200_OK)
            else:
                sputum_qs = Sputum_collection.objects.filter(participant=self.participant)

                if not self.participant.is_eligible_for_sputum():
                    return Response(data=RISK_FACTOR_PARTICIPANT_NOT_ALLOWED_TO_DO_THIS_STATION % (self.participant.pin),
                                    status=status.HTTP_404_NOT_FOUND)

                if not sputum_qs:
                    return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Colheita de Amostra"),
                                    status=status.HTTP_404_NOT_FOUND)

                response = Response(data=self.get_participant_details_for_odk(),
                                    status=status.HTTP_200_OK)
        except Symptom_Screening.DoesNotExist:
            return Response(data=PARTICIPANT_IS_MISSING_STATION % (self.participant.pin, "Rastreio de Sintomas"),
                            status=status.HTTP_404_NOT_FOUND)

        return response

    def get_participant_details_for_odk(self):
        full_name = str(self.participant.name + ' ' + self.participant.surname).ljust(60)
        age = str(self.participant.age).zfill(3)
        gender = 'M' if self.participant.gender == '1' else 'F'

        return OK + full_name + gender + age

    def validate_reception_in(self):
        try:
            ri = Reception_in.objects.get(participant=self.participant)
        except Reception_in.DoesNotExist:
            return Response(data=status.HTTP_404_NOT_FOUND, status=status.HTTP_404_NOT_FOUND)

        if ri.consent_participation == "1":
            data = self.get_participant_details_for_odk()
        else:
            data = ri.get_consent_participation_display() + " Tem consentimento para o estudo"
        return Response(data=data, status=status.HTTP_200_OK)



def validate_sample_id(pin, suffix):
    suffix = "S" + suffix
    suffixes = ("S1", "S2", "S3")
    if suffix not in suffixes:
        return Response(data='Invalid sample code suffix', status=status.HTTP_400_BAD_REQUEST)

    participant = validate_participant(pin)
    if participant is None:
        return Response(data=PARTICIPANT_DOES_NOT_EXIST % pin, status=status.HTTP_404_NOT_FOUND)

    samples = Sputum_collection.objects.filter(participant=participant)
    sample_ids = ""

    for sample in samples:
        if sample_ids != "":
            sample_ids = sample_ids + "|"

        sample_ids = sample_ids + str(sample.sample_id)

    data = sample_ids
    s1 = pin + suffixes[0]
    s2 = pin + suffixes[1]
    s3 = pin + suffixes[2]

    suffix = suffix.upper()

    if suffix == "S1":
        if s1 not in sample_ids:
            data = OK

    if suffix == "S2":
        if s1 in sample_ids and s2 not in sample_ids:
            data = OK

    if suffix == "S3":
        if s1 in sample_ids and s2 in sample_ids and s3 not in sample_ids:
            data = OK

    return Response(data=data, status=status.HTTP_200_OK)


def get_next_batch_id():
    try:
        cluster = Cluster.objects.get(status=OPEN)
    except Cluster.DoesNotExist:
        return Response(data='Cluster does not exist', status=status.HTTP_404_NOT_FOUND)

    batch = Cluster_batch.objects.filter(cluster=cluster, used=False).first()
    if batch is not None:
        batch.use_it()
        return Response(data=batch.batch_id, status=status.HTTP_200_OK)
    else:
        return Response(data="Could not find any batch id", status=status.HTTP_404_NOT_FOUND)


def evaluate_case_control_eligibility(pin):
    try:
        participant = Participant.objects.get(pin=pin)

        if participant.is_case_control:
            data = "O Participante é um caso de controle - encaminhe-o para Factores de Risco"
        else:
            data = "Encaminhe o participante para a estação de Raio-X"

        return Response(data=data, status=status.HTTP_200_OK)
    except Participant.DoesNotExist:
            return Response(data=PARTICIPANT_DOES_NOT_EXIST % pin, status=status.HTTP_404_NOT_FOUND)

