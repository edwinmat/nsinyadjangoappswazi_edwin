from django.conf.urls import url
from .views import GxAlertCreateAPIView, WidgetAPIView, ServerStatusAPIView, GxConnectCreateAPIView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    url(r'^api-token-auth/', obtain_auth_token),
    url(r'^gx/v1/gxa/test$', GxAlertCreateAPIView.as_view(), name='api-gx-alert-test'),
    url(r'^gx/v2/connect/ping$', ServerStatusAPIView.as_view(), name='api-gx-server-status'),
    url(r'^gx/v2/connect/dailydetails$', ServerStatusAPIView.as_view(), name='api-gx-daily-details'),
    url(r'^gx/v2/connect/test$', GxConnectCreateAPIView.as_view(), name='api-gx-connect-test'),
    url(r'^widget/v1/$', WidgetAPIView.as_view(), name='api-widget'),
]