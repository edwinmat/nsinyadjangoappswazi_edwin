from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from nsinya.serializers import GeneXpertSerializer
from rest_framework import status
from rest_framework.response import Response
from . import gx_utils, widget_utils


class GxAlertCreateAPIView(CreateAPIView):
    serializer_class = GeneXpertSerializer

    def perform_create(self, serializer):
        gx_utils.create_gx(serializer=serializer)


class GxConnectCreateAPIView(CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = GeneXpertSerializer

    def perform_create(self, serializer):
        gx_utils.create_gx(serializer=serializer)


class WidgetAPIView(APIView):
    request = None
    pin = None

    def get(self, request):
        request_type_mapping = {"stagestatus": self.check_stage_status,
                                "sampleid": self.validate_sample_id,
                                "batchid": widget_utils.get_next_batch_id,
                                "case_control_evaluator": self.evaluate_case_control_eligibility
                                }

        request_types = ("stagestatus", "sampleid", "batchid", "case_control_evaluator")
        request_type = request.query_params.get('requesttype', None)

        self.request = request

        if request_type not in request_types:
            return Response(data='Unknown request type', status=status.HTTP_400_BAD_REQUEST)

        self.pin = request.query_params.get('pin', None)

        return request_type_mapping[request_type]()

    def validate_sample_id(self):
        suffix = self.request.query_params.get('suffix', None)
        return widget_utils.validate_sample_id(pin=self.pin, suffix=suffix)

    def check_stage_status(self):
        form_id = self.request.query_params.get('formid', None)
        stage_status_validation = widget_utils.StageStatusValidation()

        return widget_utils.StageStatusValidation.validate_stage(self=stage_status_validation, form_id=form_id, pin=self.pin)

    def evaluate_case_control_eligibility(self):
        return widget_utils.evaluate_case_control_eligibility(pin=self.pin)


class ServerStatusAPIView(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None):
        return Response(data="200", status=status.HTTP_200_OK)
