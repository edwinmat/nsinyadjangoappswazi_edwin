from bootstrap_datepicker.widgets import DatePicker
from django import forms
from .models import Batch_reception, Reception_CRL, Culture, TSA, Medical_review, CentralCXRReading
from django.forms import TextInput, NumberInput, Select, Textarea, RadioSelect
from .custom_widgets import *


class BatchReceptionForm(forms.ModelForm):
    class Meta:
        model = Batch_reception
        exclude = ['batch_id', 'received_by', 'reception_date', 'total_received_samples']

        widgets = {
            'temperature': NumberInput(attrs={'class': 'form-control input-sm', 'autofocus': 'autofocus', 'step': "0.01"}),
        }


class SampleReceptionForm(forms.ModelForm):
    class Meta:
        model = Reception_CRL
        exclude = ['sample_id', 'batch_id', 'received_by', 'reception_date']
        fields = ['acceptability', 'sample_aspect',  'quantity', 'rejection_reason', 'problematic_sample', 'comments']

        labels = {
            "quantity": "Volume(ml)",
        }

        widgets = {
            'quantity': NumberInput(attrs={'class': 'form-control input-sm', 'min': 0, 'max': '50', 'step': "0.01"}),
            'sample_aspect': Select(attrs={'class': 'form-control input-sm'}),
            'acceptability': Select(attrs={'class': 'form-control input-sm'}),
            'rejection_reason': Select(attrs={'class': 'form-control input-sm', 'disabled':'disabled'}),
            'problematic_sample': Select(attrs={'class': 'form-control input-sm', 'disabled':'disabled'}),
            'comments': Textarea(attrs={'class': 'form-control input-sm','rows':3}),
        }

    def __init__(self, *args, **kwargs):
        super(SampleReceptionForm, self).__init__(*args, **kwargs)
        self.fields['rejection_reason'].required = False
        self.fields['problematic_sample'].required = False


class CultureForm(forms.ModelForm):
    class Meta:
        model = Culture
        exclude = ['technician_name', 'result_date', 'update_date', 'validated_by_name', 'validated_by', 'validation_date']

        widgets = {
            'sample_id': TextInput(attrs={'class': 'form-control input-sm', 'readonly':'readonly'}),
            'culture_result': Select(attrs={'class': 'form-control input-sm'}),
            'positivity_week': Select(attrs={'class': 'form-control input-sm'}),
            'gradation': Select(attrs={'class': 'form-control input-sm', 'disabled':'disabled'}),
            'tube': Select(attrs={'class': 'form-control input-sm'}),
        }

    def __init__(self, *args, **kwargs):
        super(CultureForm, self).__init__(*args, **kwargs)
        #self.fields['positivity_week'].required = False
        self.fields['gradation'].required = False


class TSAForm(forms.ModelForm):
    class Meta:
        model = TSA
        exclude = ['sample_id', 'technician_code', 'result_date', 'validated_by', 'validation_date']

        labels = {
            "blood_result": "Blood Agar Plate",
            "TSA_1_H_result": "H",
            "TSA_1_R_result": "R",
            "TSA_1_Z_result": "Z",
            "TSA_1_E_result": "E",
            "TSA_1_S_result": "S",
            "TSA_2_Am_result": "AM",
            "TSA_2_Ka_result": "KA",
            "TSA_2_Cm_result": "CM",
            "TSA_2_Ofl_result": "OFL",
        }

        widgets = {
            'blood_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_1_H_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_1_R_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_1_Z_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_1_E_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_1_S_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_2_Am_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_2_Ka_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_2_Cm_result': Select(attrs={'class': 'form-control input-sm'}),
            'TSA_2_Ofl_result': Select(attrs={'class': 'form-control input-sm'}),
            'acceptability': Select(attrs={'class': 'form-control input-sm'}),
        }

    def __init__(self, *args, **kwargs):
        super(TSAForm, self).__init__(*args, **kwargs)


class MedicalReviewForm(forms.ModelForm):
    class Meta:
        model = Medical_review
        exclude = ['participant_id', 'review_date', 'panel_representative']

        widgets = {
            'tb_case': Select(attrs={'class': 'form-control input-sm'}),
            'remarks': Textarea(attrs={'class': 'form-control input-sm'}),
        }

    def __init__(self, *args, **kwargs):
        super(MedicalReviewForm, self).__init__(*args, **kwargs)
        self.fields['remarks'].required = False


class CentralCXRReadingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CentralCXRReadingForm, self).__init__(*args, **kwargs)

    class Meta:
        model = CentralCXRReading
        exclude = ['participant', 'creation_date', 'update_date']

        widgets = {
            'cxr_reading_result': Select(attrs={'class': 'form-control input-sm','required':True}),
            'cavity': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'infiltrate': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'lung_nodules': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'pleural_effusion': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'mediastinal_lymphadenopathy': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'hilar_lymphadenopathy': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'discipleship': HorizontalRadioSelect(attrs={'disabled': 'disabled'}),
            'cxr_final_reading_result': Select(attrs={'class': 'form-control input-sm', 'disabled': 'disabled'}),
            'reading_status': Select(attrs={'class': 'form-control input-sm'}),
        }
